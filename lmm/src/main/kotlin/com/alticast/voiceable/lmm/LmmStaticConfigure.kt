/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm
      Date  : 19. 3. 7
     Author : kimjh 
*/
package com.alticast.voiceable.lmm

import org.slf4j.LoggerFactory
import java.util.concurrent.atomic.AtomicBoolean

object LmmStaticConfigure {
    private val logger = LoggerFactory.getLogger(LmmStaticConfigure::class.java)

    private val isMasterMode = AtomicBoolean(true)

    fun setMasterMode(mode: Boolean) {
        logger.info("setMasterMode: $mode")
        isMasterMode.set(mode)
    }

    fun isMasterMode() = isMasterMode.get()
}