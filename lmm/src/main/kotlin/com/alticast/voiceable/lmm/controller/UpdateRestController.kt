/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.controller
      Date  : 19. 3. 18
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.controller

import com.alticast.voiceable.lmm.service.IngestLanguageModel
import com.alticast.voiceable.lmm.service.cached.LanguageModelManagementService
import com.alticast.voiceable.lmm.service.exchange.LanguageModelExchanger
import com.alticast.voiceable.lmm.service.exchange.UpdateEntityInfo
import com.alticast.voiceable.lmm.utils.FileUtils
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class UpdateRestController {
    private val logger = LoggerFactory.getLogger(UpdateRestController::class.java)

    @Value("\${lmm.temp.path}")
    lateinit var workingPath: String

    @Value("\${lmm.source.path}")
    lateinit var entitySourcePath: String

    @Autowired
    lateinit var modelExchanger: LanguageModelExchanger

    @Autowired
    lateinit var modelManagementService: LanguageModelManagementService

    @Autowired
    lateinit var ingestLanguageModel: IngestLanguageModel

    @GetMapping("/operation/update_entities/{variant}/{language}")
    fun updateEntities(@PathVariable("variant") variant: String,
                       @PathVariable("language") language: String,
                       @RequestParam("deploy", required = false) deploy: Boolean = false): Any {

        logger.debug("update auto-gen entities from external services")
        val updateList = FileUtils.copyDataFrom(variant, language, entitySourcePath, workingPath)
        val updated = updateList.isNotEmpty()

        if (updated) {
            logger.debug("auto-gen entities are updating...")
            val dataList = updateList.map {
                val wName = it.replace(entitySourcePath, workingPath)
                val wNames = wName.replace(workingPath, "").drop(1).split("/")

                val appName = wNames[0]
                val dataType = wNames[1]
                val dataName = wNames[2]

                UpdateEntityInfo("$workingPath/$variant/$language/$appName/$dataType/$dataName",
                        variant, language, appName, dataType, dataName)
            }
            modelExchanger.updateEntities(dataList)
            logger.debug("auto-gen entities are updated!")
        }

        return mapOf("updated" to updated)
    }

    @GetMapping("/operation/deploy_last")
    fun deployLastRevision(@RequestParam("forced", required = false) forced: Boolean?,
                           @RequestParam("full", required = false) full: Boolean?): Any {
        logger.debug("deploy last revision - forced($forced) full($full) (forced/full not used - check in ingest modules)")

        modelManagementService.deployRevision(null, null)?.run {
            ingestLanguageModel.ingestLanguageModel(this)
        }

        return mapOf("deploy" to "ok")
    }
}