package com.alticast.voiceable.lmm.service

import com.alticast.voiceable.lmm.repository.datamodel.ActionParam
import com.alticast.voiceable.lmm.repository.datamodel.ActionParamInfo
import com.alticast.voiceable.lmm.repository.datamodel.ActionParamInfoData
import com.alticast.voiceable.lmm.repository.datamodel.ActionParamIntentsDto
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValues
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.io.InputStream
import java.util.Stack

class ActionParamIntents(val actionKey: String, val actionParams: List<ActionParamData<*>>)

enum class ActionParamDataType {
    STRING,
    INTEGER,
    BOOLEAN,
    RESERVED
}

class ActionParamData<T>(val value: T, val type: ActionParamDataType) {
    override fun toString() = "ActionParamData($type/$value)"
}

@Service
class ActionParamService {
    @Autowired
    lateinit var dataService: DataService

    fun loadActionParam(variant: String, input: InputStream): Int {
        val data = try {
            val objectMapper = ObjectMapper(YAMLFactory()).registerModule(KotlinModule())
            val yamlParser = YAMLFactory().createParser(input.bufferedReader())

            objectMapper.readValues<ActionParamIntentsDto>(yamlParser).readAll()
        } catch (e: Exception) {
            logger.error("fail to load action param named : $variant")
            return 0
        }

        var loadingCount = 0
        data.forEach {
            val userVariant = it.variant ?: variant
            dataService.findAllActionParamsByVariant(userVariant)?.let {
                dataService.deleteAllActionParams(it)
            }

            loadingCount += it.intents?.map { intent ->
                ActionParam(name = intent.name, variant = userVariant, script = intent.script, help = intent.help
                        ?: "", params = ActionParamInfoData(intent.group, intent.params))
            }?.let {
                dataService.saveAllActionParams(it)
                it.count()
            } ?: 0
        }
        setActionParams(dataService.findAllActionParams())

        return loadingCount
    }

    companion object {
        private val logger = LoggerFactory.getLogger(ActionParamService::class.java)

        private val actionParamsList = mutableListOf<ActionParam>()
        private fun getActionParamsKey(variant: String, key: String) = "$variant.$key"
        fun setActionParams(actionParams: List<ActionParam>?) {
            actionParams ?: return

            actionParamsList.clear()
            actionParamsList.addAll(actionParams)
        }

        private fun getActionKeyFromAction(action: String): String? {
            val ap = actionParamsList.find { it.script == action } ?: return null

            return getActionParamsKey(ap.variant, ap.name)
        }

        fun getActionParams(variant: String?): List<ActionParam> {
            return actionParamsList.filter { it.variant == variant || variant == null }
        }

        private fun getParamDataFromParamString(refData: ActionParamInfo, given: String): ActionParamData<*> {
            val paramData: ActionParamData<out Any>
            if (given == "null") {
                paramData = ActionParamData("null", ActionParamDataType.RESERVED)
            } else
                paramData = when (refData.type.toLowerCase()) {
                    "integer" -> {
                        ActionParamData(given.toInt(), ActionParamDataType.INTEGER)
                    }
                    "entity" -> {
                        ActionParamData(given, ActionParamDataType.STRING)
                    }
                    "boolean" -> {
                        ActionParamData(given.toLowerCase() == "true", ActionParamDataType.BOOLEAN)
                    }
                    "string" -> {
                        ActionParamData(given, ActionParamDataType.STRING)
                    }
                    else -> {
                        ActionParamData(given, ActionParamDataType.STRING)
                    }
                }

            return paramData
        }

        fun getActionParamFromParamList(variant: String, actionKey: String, paramList: List<String>?): List<ActionParamData<*>> {
            val actionParams = getActionParams(variant).find { "$variant.${it.name}" == actionKey } ?: return listOf()
            actionParams.params ?: return listOf()

            val paramDataList = mutableListOf<ActionParamData<*>>()
            if (actionParams.params.group != null) {
                val groupParams = actionParams.params.group.params
                if (groupParams != null) {
                    paramList?.forEachIndexed { index, param ->
                        paramDataList.add(getParamDataFromParamString(groupParams[index % groupParams.count()], param))
                    }
                }
            } else
                if (actionParams.params.params != null) {
                    paramList?.forEachIndexed { index, param ->
                        val params = actionParams.params.params
                        if (params.count() > index)
                            paramDataList.add(getParamDataFromParamString(params[index], param))
                    }
                }
            //val actionParams = getActionParams(variant).find { "$variant.${it.name}" == actionKey }?.params?.params
            //        ?: return listOf()
            //
            //val paramDataList = mutableListOf<ActionParamData<*>>()
            //paramList?.forEachIndexed { idx, param ->
            //    if (actionParams.count() > idx)
            //        paramDataList.add(getParamDataFromParamString(actionParams[idx], param))
            //}

            return paramDataList
        }

        fun getActionFromActionParam(actionParam: ActionParamIntents?): String {
            actionParam ?: return ""

            val actionKeys = actionParam.actionKey.split(".")
            val variant = actionKeys.first()
            val actionName = actionKeys.last()
            val script = actionParamsList.find { it.variant == variant && it.name == actionName }?.script ?: return ""

            val params = actionParam.actionParams.map {
                when (it.type) {
                    ActionParamDataType.INTEGER -> {
                        (it.value as Int).toString()
                    }
                    ActionParamDataType.BOOLEAN -> {
                        (it.value as Boolean).toString()
                    }
                    ActionParamDataType.RESERVED -> {
                        it.value as String
                    }
                    else -> {
                        "\"" + it.value as String + "\""
                    }
                }
            }

            return "$script(${params.joinToString(", ")})"
        }

        fun getActionParamFromAction(action: String): ActionParamIntents? {
            if (action.isEmpty()) return null

            val regex = """(\w+\.\w+)|(\(.*\))""".toRegex()
            val actionTrimmed = action.trim()
            val reg = regex.findAll(actionTrimmed).map {
                it.value
            }.toList()

            if (reg.isEmpty()) {
                logger.warn("actionParam Meta is Empty - $action")
                return null
            }
            val actionKey = getActionKeyFromAction(reg.first()) ?: let {
                logger.warn("Can't find action key from action method - $action - ${reg.first()}")
                return null
            }

            val opsStack = Stack<Char>()
            val builder = StringBuilder()
            val parameters = reg.last().drop(1).dropLast(1)
            val parsedParams = mutableListOf<String>()

            for (ch in parameters) {
                if (ch in setOf('\'', '"')) {
                    if (!opsStack.empty() && opsStack.peek() == ch)
                        opsStack.pop()
                    else
                        opsStack.push(ch)
                }

                if (ch == ',') {
                    if (opsStack.empty()) {
                        parsedParams.add(builder.toString())
                        builder.setLength(0)
                    } else {
                        builder.append(ch)
                    }
                } else
                    builder.append(ch)
            }
            if (builder.isNotEmpty())
                parsedParams.add(builder.toString())

            logger.debug("ParsedParameters: [$parameters] to [$parsedParams]")
            val actionParams = parsedParams.mapNotNull { s ->
                val testParam = s.trim()
                when {
                    testParam.isEmpty() -> null
                    testParam == "null" -> ActionParamData("null", ActionParamDataType.RESERVED)
                    testParam.startsWith('"') -> ActionParamData(testParam.drop(1).dropLast(1), ActionParamDataType.STRING)
                    testParam in setOf("true", "false") -> ActionParamData(testParam == "true", ActionParamDataType.BOOLEAN)
                    testParam.all { it.isDigit() } -> ActionParamData(testParam.toInt(), ActionParamDataType.INTEGER)
                    else -> {
                        logger.warn("Unknown action string : $action)")
                        null
                    }
                }
            }

            //val testAction = getActionFromActionParam(paramIntents)
            //println(testAction)
            return ActionParamIntents(actionKey, actionParams)
        }
    }
}