/*
    Copyright (C) 2018 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.langmo
      Date  : 18. 9. 19
     Author : kimjh 
*/
package com.alticast.voiceable.lmm

import com.alticast.text.entity.DateTimeEntity
import com.alticast.text.entity.EntityParser
import com.alticast.text.entity.NumberEntity
import com.alticast.text.entity.NumericEntity
import com.alticast.text.entity.PasswordEntity
import com.alticast.text.entity.SystemEntityManager
import com.alticast.text.entity.TimeEntity
import com.alticast.text.entity.WildCardEntity
import com.alticast.voiceable.lmm.service.exchange.LanguageModelExchanger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.stereotype.Component

@Component
class ApplicationReadyListener : ApplicationListener<ContextRefreshedEvent> {
    private val logger = LoggerFactory.getLogger(ApplicationReadyListener::class.java)

    @Value("\${lmm.master}")
    private val lmmMaster: Boolean = true

    @Autowired
    lateinit var languageModelExchanger: LanguageModelExchanger

    private fun registerBaseEntities() {
        //  TODO: 외부에서 읽어서 설정할수 있게 해야함
        val passwordEntity = PasswordEntity("password", 4)
        SystemEntityManager.addParser(EntityParser("password", passwordEntity))
//        SystemEntityManager.addParser(new EntityParser("age", new AgeEntity("age")));
        SystemEntityManager.addParser(EntityParser("age", NumericEntity("age")))
        SystemEntityManager.addParser(EntityParser("number", NumericEntity("number")))
        SystemEntityManager.addParser(EntityParser("year", NumericEntity("year", 1800, 2030)))
        SystemEntityManager.addParser(EntityParser("vodnumber", NumericEntity("vodnumber", 1)))
        SystemEntityManager.addParser(EntityParser("time", TimeEntity("time")))
        SystemEntityManager.addParser(EntityParser("datetime", DateTimeEntity("datetime")))
        SystemEntityManager.addParser(EntityParser("number.6f", NumberEntity("number.6f", 6, true)))
        SystemEntityManager.addParser(EntityParser("number.6", NumberEntity("number.6", 6, false)))
        SystemEntityManager.addParser(EntityParser("number.4f", NumberEntity("number.4f", 4, true)))
        SystemEntityManager.addParser(EntityParser("number.4", NumberEntity("number.4", 4, false)))
        SystemEntityManager.addParser(EntityParser("number.3f", NumberEntity("number.3f", 3, true)))
        SystemEntityManager.addParser(EntityParser("number.3", NumberEntity("number.3", 3, false)))
        SystemEntityManager.addParser(EntityParser("number.2f", NumberEntity("number.2f", 2, true)))
        SystemEntityManager.addParser(EntityParser("number.2", NumberEntity("number.2", 2, false)))

        SystemEntityManager.addParser(EntityParser("fnumber", NumericEntity("fnumber")))

        SystemEntityManager.addParser(EntityParser("*1", WildCardEntity("*1", 1, false)))
        SystemEntityManager.addParser(EntityParser("*2", WildCardEntity("*2", 2, false)))
        SystemEntityManager.addParser(EntityParser("*3", WildCardEntity("*3", 3, false)))
        SystemEntityManager.addParser(EntityParser("*4", WildCardEntity("*4", 4, false)))
        SystemEntityManager.addParser(EntityParser("*5", WildCardEntity("*5", 5, false)))
        SystemEntityManager.addParser(EntityParser("*6", WildCardEntity("*6", 6, false)))
        SystemEntityManager.addParser(EntityParser("*query", WildCardEntity("*query", 20, true)))
    }

    override fun onApplicationEvent(event: ContextRefreshedEvent) {
        LmmStaticConfigure.setMasterMode(lmmMaster)

        registerBaseEntities()

        languageModelExchanger.createReleaseWorkspace()
    }
}