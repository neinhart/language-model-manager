/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.service
      Date  : 19. 3. 12
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.service

import com.alticast.voiceable.lmm.repository.ActionParamRepository
import com.alticast.voiceable.lmm.repository.LanguageModelDictionaryItemRepository
import com.alticast.voiceable.lmm.repository.LanguageModelDictionaryRepository
import com.alticast.voiceable.lmm.repository.LanguageModelEntityItemRepository
import com.alticast.voiceable.lmm.repository.LanguageModelEntityRepository
import com.alticast.voiceable.lmm.repository.LanguageModelPrivateRepository
import com.alticast.voiceable.lmm.repository.LanguageModelRepository
import com.alticast.voiceable.lmm.repository.LanguageModelRevisionRepository
import com.alticast.voiceable.lmm.repository.LanguageModelStoryItemRepository
import com.alticast.voiceable.lmm.repository.LanguageModelStoryRepository
import com.alticast.voiceable.lmm.repository.LanguageModelWorkspaceRepository
import com.alticast.voiceable.lmm.repository.datamodel.ActionParam
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModel
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelDictionary
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelDictionaryItem
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelEntity
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelEntityItem
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelPrivate
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelRevision
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelStory
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelStoryItem
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelWorkspace
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class DataService {
    @Autowired
    lateinit var languageModelWorkspaceRepository: LanguageModelWorkspaceRepository

    @Autowired
    lateinit var languageModelRepository: LanguageModelRepository

    @Autowired
    lateinit var languageModelEntityRepository: LanguageModelEntityRepository

    @Autowired
    lateinit var languageModelEntityItemRepository: LanguageModelEntityItemRepository

    @Autowired
    lateinit var languageModelRevisionRepository: LanguageModelRevisionRepository

    @Autowired
    lateinit var languageModelDictionaryRepository: LanguageModelDictionaryRepository

    @Autowired
    lateinit var languageModelDictionaryItemRepository: LanguageModelDictionaryItemRepository

    @Autowired
    lateinit var languageModelPrivateRepository: LanguageModelPrivateRepository

    @Autowired
    lateinit var languageModelStoryRepository: LanguageModelStoryRepository

    @Autowired
    lateinit var languageModelStoryItemRepository: LanguageModelStoryItemRepository

    @Autowired
    lateinit var actionParamRepository: ActionParamRepository

    fun findWorkspaceByEditable(editable: Boolean): LanguageModelWorkspace? {
        return languageModelWorkspaceRepository.findByEditable(editable)
    }

    fun findWorkspaceListByIds(idList: List<Long>?): List<LanguageModelWorkspace> {
        return idList?.let {
            languageModelWorkspaceRepository.findAllById(it)
        } ?: let {
            languageModelWorkspaceRepository.findAll()
        }
    }

    fun findWorkspaceById(id: Long): Optional<LanguageModelWorkspace> {

        return languageModelWorkspaceRepository.findById(id)
    }

    fun findRevision(workspaceId: Long, revision: Long?): LanguageModelRevision? {
        val revisionList = languageModelRevisionRepository.findAllByLanguageModelWorkspaceId(workspaceId)

        return if (revision == null) {
            revisionList?.find { it.working }
        } else {
            revisionList?.find { it.id == revision }
        }
    }

    fun findRevision(revision: Long): LanguageModelRevision? {
        return languageModelRevisionRepository.findById(revision).get()
    }

    fun findAllRevisionsByWorkspace(workspaceId: Long): List<LanguageModelRevision>? {
        return languageModelRevisionRepository.findAllByLanguageModelWorkspaceId(workspaceId)
    }

    fun findWorkingRevision(workspaceId: Long): LanguageModelRevision? {
        return findAllRevisionsByWorkspace(workspaceId)?.find { it.working }
    }

    fun findAllLanguageModels(): List<LanguageModel> {
        return languageModelRepository.findAll()
    }

    fun findLanguageModelListByRevisionId(revisionId: Long): List<LanguageModel>? {
        return languageModelRepository.findAllByLanguageModelRevisionId(revisionId)
    }

    fun findLanguageModelByIdVariantLanguageAppName(revId: Long, variant: String, language: String, appName: String): LanguageModel? {
        return languageModelRepository.findByLanguageModelRevisionIdAndVariantAndLanguageAndAppName(revId, variant, language, appName)
    }

    fun findLanguageModelByIdVariantLanguage(revId: Long, variant: String, language: String): List<LanguageModel>? {
        return languageModelRepository.findAllByLanguageModelRevisionIdAndVariantAndLanguage(revId, variant, language)
    }

    fun findAllEntitiesByLanguageModel(modelId: Long): List<LanguageModelEntity>? {
        return languageModelEntityRepository.findAllByLanguageModelId(modelId)
    }

    fun findAllEntitiesByLanguageModelAndDataName(modelId: Long, dataName: String): List<LanguageModelEntity>? {
        return languageModelEntityRepository.findAllByLanguageModelIdAndName(modelId, dataName)
    }

    fun findAllEntityItemsByEntity(id: Long): List<LanguageModelEntityItem>? {
        return languageModelEntityItemRepository.findAllByLanguageModelEntityId(id)
    }

    fun findEntityById(id: Long): LanguageModelEntity? {
        return languageModelEntityRepository.findById(id).get()
    }

    fun findAllEntityItemByIds(idList: List<Long>): List<LanguageModelEntityItem>? {
        return languageModelEntityItemRepository.findAllByIdIn(idList)
    }

    fun findEntityItemById(id: Long): LanguageModelEntityItem? {
        return languageModelEntityItemRepository.findById(id).get()
    }

    fun findDictionaryById(id: Long): LanguageModelDictionary? {
        return languageModelDictionaryRepository.findById(id).get()
    }

    fun findAllDictionariesByLanguageModel(modelId: Long): List<LanguageModelDictionary>? {
        return languageModelDictionaryRepository.findAllByLanguageModelId(modelId)
    }

    fun findAllDictionaryItemsByDictionary(id: Long): List<LanguageModelDictionaryItem>? {
        return languageModelDictionaryItemRepository.findAllByLanguageModelDictionaryId(id)
    }

    fun findStoryById(id: Long): LanguageModelStory? {
        return languageModelStoryRepository.findById(id).get()
    }

    fun findAllStoriesByLanguageModel(modelId: Long): List<LanguageModelStory>? {
        return languageModelStoryRepository.findAllByLanguageModelId(modelId)
    }

    fun findAllStoriesByUser(userId: Long): List<LanguageModelStory>? {
        return languageModelStoryRepository.findAllByLockById(userId)
    }

    fun findAllStoryItemsByStory(storyId: Long): List<LanguageModelStoryItem>? {
        return languageModelStoryItemRepository.findAllByLanguageModelStoryId(storyId)
    }

    fun findAllPrivatesByLanguageModel(modelId: Long): List<LanguageModelPrivate>? {
        return languageModelPrivateRepository.findAllByLanguageModelId(modelId)
    }

    fun save(ws: LanguageModelWorkspace): LanguageModelWorkspace {
        return languageModelWorkspaceRepository.save(ws)
    }

    fun save(en: LanguageModelEntity): LanguageModelEntity {
        return languageModelEntityRepository.save(en)
    }

    fun save(eni: LanguageModelEntityItem): LanguageModelEntityItem {
        return languageModelEntityItemRepository.save(eni)
    }

    fun save(lm: LanguageModel): LanguageModel {
        return languageModelRepository.save(lm)
    }

    fun save(lmr: LanguageModelRevision): LanguageModelRevision {
        return languageModelRevisionRepository.save(lmr)
    }

    fun save(dict: LanguageModelDictionary): LanguageModelDictionary {
        return languageModelDictionaryRepository.save(dict)
    }

    fun save(dictItem: LanguageModelDictionaryItem): LanguageModelDictionaryItem {
        return languageModelDictionaryItemRepository.save(dictItem)
    }

    fun save(priv: LanguageModelPrivate): LanguageModelPrivate {
        return languageModelPrivateRepository.save(priv)
    }

    fun save(story: LanguageModelStory): LanguageModelStory {
        return languageModelStoryRepository.save(story)
    }

    fun save(story: LanguageModelStoryItem): LanguageModelStoryItem {
        return languageModelStoryItemRepository.save(story)
    }

    fun delete(ws: LanguageModelWorkspace) {
        languageModelWorkspaceRepository.delete(ws)
    }

    fun delete(en: LanguageModelEntity) {
        languageModelEntityRepository.delete(en)
    }

    fun delete(eni: LanguageModelEntityItem) {
        languageModelEntityItemRepository.delete(eni)
    }

    fun deleteEntityItemByIds(idList: List<Long>) {
        languageModelEntityItemRepository.deleteByIdIn(idList)
    }

    fun delete(lm: LanguageModel) {
        languageModelRepository.delete(lm)
    }

    fun delete(lmr: LanguageModelRevision) {
        languageModelRevisionRepository.delete(lmr)
    }

    fun delete(dict: LanguageModelDictionary) {
        languageModelDictionaryRepository.delete(dict)
    }

    fun delete(dictItem: LanguageModelDictionaryItem) {
        languageModelDictionaryItemRepository.delete(dictItem)
    }

    fun delete(priv: LanguageModelPrivate) {
        languageModelPrivateRepository.delete(priv)
    }

    fun delete(story: LanguageModelStory) {
        languageModelStoryRepository.delete(story)
    }

    fun delete(story: LanguageModelStoryItem) {
        languageModelStoryItemRepository.delete(story)
    }

    fun findAllActionParamsByVariant(variant: String): List<ActionParam>? {
        return actionParamRepository.findAllByVariant(variant)
    }

    fun deleteAllActionParams(params: List<ActionParam>) {
        actionParamRepository.deleteAll(params)
    }

    fun saveAllActionParams(params: List<ActionParam>) {
        actionParamRepository.saveAll(params)
    }

    fun findAllActionParams(): List<ActionParam>? {
        return actionParamRepository.findAll()
    }
}