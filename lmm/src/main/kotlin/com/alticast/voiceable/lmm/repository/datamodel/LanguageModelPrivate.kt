/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.repository.datamodel
      Date  : 19. 3. 12
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.repository.datamodel

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.io.File
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Lob
import javax.persistence.ManyToOne

@Entity
data class LanguageModelPrivate(
        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0,

        var name: String = "",
        var fileDataKey: String = "",
        @Lob
        var data: String = "",

        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @OnDelete(action = OnDeleteAction.CASCADE)
        @JsonIgnore
        private var languageModel: LanguageModel? = null
) {
    companion object {
        val baseDir = "extras"
        fun createPrivateModel(parentModel: LanguageModel, dataKey: String, dataFile: File,
                               onPrivateData: (model: LanguageModelPrivate, data: String) -> Unit): LanguageModelPrivate {
            val data = dataFile.bufferedReader().use { it.readText() }

            if (dataKey.isEmpty())
                return LanguageModelPrivate(fileDataKey = dataKey, languageModel = parentModel, data = data, name = dataFile.name)

            val privateModel = LanguageModelPrivate(fileDataKey = dataKey, languageModel = parentModel, data = "", name = dataFile.name)
            onPrivateData(privateModel, data)

            return privateModel
        }

        fun writePrivateModel(dataDir: File, privateModel: LanguageModelPrivate) {
            val dstFile = File(dataDir, "$baseDir/${privateModel.name}")
            if (!dstFile.parentFile.isDirectory)
                dstFile.parentFile.mkdirs()

            dstFile.bufferedWriter().use { bw ->
                bw.write(privateModel.data)
            }
        }

        fun createLanguageModelPrivate(name: String, fileDataKey: String, data: String, model: LanguageModel): LanguageModelPrivate {
            return LanguageModelPrivate(name = name, fileDataKey = fileDataKey, data = data, languageModel = model)
        }
    }
}