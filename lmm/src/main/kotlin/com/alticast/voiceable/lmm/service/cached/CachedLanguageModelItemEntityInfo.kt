package com.alticast.voiceable.lmm.service.cached

import com.alticast.text.utils.StringUtils
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelEntity

class CachedLanguageModelItemEntityInfo(val entity: LanguageModelEntity) : CachedLanguageModelItem {
    val name = entity.name
    val fileDataKey = entity.fileDataKey
    val fields = entity.headers.split(":").flatMap {
        if (it == "@fields") listOf("speech") else it.split("|")
    }.toMutableList()

    data class EntityItemInfo(val id: Long, var data: List<String>, var state: DataUpdateMode)

    val itemList = mutableListOf<EntityItemInfo>()

    fun addItem(id: Long, data: String): CachedLanguageModelItemEntityInfo {
        val splitted = StringUtils.splitBy(data, ':')
        val speech = splitted[0].replace("\\", "")
        val itemData = listOf(speech) + StringUtils.splitBy(splitted.drop(1).joinToString(":"), '|').map { it.replace("\\", "") }

        itemList.add(EntityItemInfo(id, itemData, DataUpdateMode.asitwas))

        return this
    }

    fun addItem(index: Int, data: List<String>): CachedLanguageModelItemEntityInfo {
        val info = itemList[index]

        itemList.add(index, EntityItemInfo(-1, data, DataUpdateMode.insert))

        return this
    }

    fun updateItem(index: Int, data: List<String>): CachedLanguageModelItemEntityInfo {
        val info = itemList[index]

        info.data = data
        info.state = DataUpdateMode.update

        return this
    }

    fun getEntityItemDbData(itemInfo: EntityItemInfo): String {
        return itemInfo.data.first() + ":" + itemInfo.data.drop(1).joinToString("|") { it.replace("|", "\\") }
    }

    fun getEntityItemDbData(data: List<String>): String {
        return data.first() + ":" + data.drop(1).joinToString("|") {
            it.replace("|", "\\|")
        }
    }
}