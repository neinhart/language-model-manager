/*
    Copyright (C) 2018 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.languagemodelmanager.jwt
      Date  : 18. 9. 18
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.jwt

import com.alticast.voiceable.lmm.repository.datamodel.User
import com.alticast.voiceable.lmm.service.UserService
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Pointcut
import org.aspectj.lang.reflect.MethodSignature
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils
import org.springframework.web.bind.annotation.ResponseStatus
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


/**
 * Aspect whose goal is to check automatically that methods
 * having a @ApiKeySecured annotation are correctly accessed
 * by users having a valid API Key (JWT).

 * A check against the user service is done to find the user
 * having the api key passed as request header/parameter.

 * If the API Key is valid the annotated method is executed,
 * otherwise the response is set with an UNAUTHORIZED status and the annotated
 * method is not executed.
 */
@Aspect
@Component
class ApiKeySecuredAspect(@Autowired val userService: UserService) {
    private val logger = LoggerFactory.getLogger(ApiKeySecuredAspect::class.java)

    @Autowired
    lateinit var request: HttpServletRequest

    @Pointcut(value = "execution(@com.alticast.voiceable.lmm.jwt.ApiKeySecured * *.*(..))")
    fun securedApiPointcut() {
    }

    private fun findUserByApiKey(apiKey: String?, anno: ApiKeySecured): User? {
        if (!anno.mandatory && apiKey.isNullOrEmpty())
            return User()

        if (apiKey.isNullOrEmpty()) return null

        val user = userService.findByToken(apiKey)?.let {
            if (userService.validToken(apiKey, it))
                it
            else if (!anno.mandatory)
                User()
            else
                null
        }

        return user
    }

    @Around("securedApiPointcut()")
    @Throws(Throwable::class)
    fun aroundSecuredApiPointcut(joinPoint: ProceedingJoinPoint): Any? {
        if (request.method == "OPTIONS")
            return joinPoint.proceed()

        // see the ExposeResponseInterceptor class.
        val response = request.getAttribute(ExposeResponseInterceptor.KEY) as HttpServletResponse

        // check for needed roles
        val signature = joinPoint.signature as MethodSignature
        val method = signature.method
        val anno = method.getAnnotation(ApiKeySecured::class.java)

        val apiKey = request.getHeader("Authorization")?.replace("Token ", "")
        val user = findUserByApiKey(apiKey, anno) ?: let {
            logger.info("Authorization: {} is an invalid JWT.", apiKey, HttpServletResponse.SC_UNAUTHORIZED)
            issueError(response)
            return null
        }

        logger.info("User is: ${user.email}")
        userService.setCurrentUser(user)
        try {
            val result = joinPoint.proceed()
            // remove user from thread local
            userService.clearCurrentUser()

            return result
        } catch (e: Throwable) {
            // check for custom exception
            val rs = e.javaClass.getAnnotation(ResponseStatus::class.java)
            if (rs != null) {
                logger.error("ERROR accessing resource, reason: '{}', status: {}.",
                        if (StringUtils.isEmpty(e.message)) rs.reason else e.message,
                        rs.value)
            } else {
                logger.error("ERROR accessing resource")
            }
            throw e
        }

    }

    private fun issueError(response: HttpServletResponse) {
        setStatus(response, HttpServletResponse.SC_UNAUTHORIZED)
        response.setHeader("Authorization", "You shall not pass without providing a valid API Key")
        response.writer.write("{\"errors\": {\"Authorization\": [\"You must provide a valid Authorization header.\"]}}")
        response.writer.flush()
    }

    fun setStatus(response: HttpServletResponse?, sc: Int) {
        if (response != null)
            response.status = sc
    }
}
