/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.controller
      Date  : 19. 3. 26
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.controller

import com.alticast.voiceable.lmm.exceptions.InvalidUserException
import com.alticast.voiceable.lmm.jwt.ApiKeySecured
import com.alticast.voiceable.lmm.service.UserService
import com.alticast.voiceable.lmm.service.cached.ContextOrderInfo
import com.alticast.voiceable.lmm.service.cached.CreateContext
import com.alticast.voiceable.lmm.service.cached.CreateIntentInfo
import com.alticast.voiceable.lmm.service.cached.LanguageModelManagementService
import com.alticast.voiceable.lmm.service.cached.ModifyContext
import com.alticast.voiceable.lmm.service.cached.UpdateIntentInfo
import io.swagger.annotations.ApiOperation
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class LanguageModelContextRestController {
    private val logger = LoggerFactory.getLogger(LanguageModelContextRestController::class.java)

    @Autowired
    lateinit var languageModelManagementService: LanguageModelManagementService

    @Autowired
    lateinit var userService: UserService

    @ApiOperation("Get context list")
    @ApiKeySecured
    @GetMapping("/workspace/{id}/context")
    fun getContextList(@PathVariable("id") id: Long,
                       @RequestParam("rev", required = false) rev: Long?): Any {
        val contextList = languageModelManagementService.findAllContextByRevision(id, rev)

        return mapOf("contexts" to contextList.sortedBy { it.priority })
    }

    @ApiOperation("change priority regarding order")
    @ApiKeySecured
    @PostMapping("/workspace/{id}/context/order")
    fun setContextOrder(@PathVariable("id") id: Long,
                        @Valid @RequestBody contextOrderList: List<ContextOrderInfo>): Any {

        val contextList = languageModelManagementService.setContextOrder(id, contextOrderList)

        return mapOf("contexts" to contextList.sortedBy { it.priority })
    }

    @ApiOperation("create context")
    @PostMapping("/workspace/{id}/context")
    fun createContext(@PathVariable("id") id: Long,
                      @Valid @RequestBody params: CreateContext): Any {
        val context = languageModelManagementService.createContext(id, params)

        return mapOf("context" to context)
    }

    @ApiOperation("edit context info")
    @PatchMapping("/workspace/{id}/context/{cid}")
    fun modifyContext(@PathVariable("id") id: Long,
                      @PathVariable("cid") cid: Long,
                      @Valid @RequestBody params: ModifyContext): Any {

        val currentUser = userService.currentUser() ?: throw InvalidUserException("can't find current user")
        val context = languageModelManagementService.modifyContext(id, cid, params, currentUser)

        return mapOf("context" to context)
    }

    @ApiOperation("remove context")
    @DeleteMapping("/workspace/{id}/context/{cid}")
    fun removeContext(@PathVariable("id") id: Long,
                      @PathVariable("cid") cid: Long): Any {
        val currentUser = userService.currentUser() ?: throw InvalidUserException("can't find current user")
        val res = languageModelManagementService.removeContext(id, cid, currentUser)

        return mapOf("context" to res)
    }

    @ApiOperation("(un)lock context by user")
    @PutMapping("/workspace/{id}/context/{cid}")
    fun lockUnlockContext(@PathVariable("id") id: Long,
                          @PathVariable("cid") cid: Long,
                          lock: Boolean): Any {
        val currentUser = userService.currentUser() ?: throw InvalidUserException("can't find current user")
        val context = languageModelManagementService.lockContextByUser(id, cid, lock, currentUser)

        return mapOf("context" to context)
    }

    @ApiOperation("move change context a to b")
    @PutMapping("/workspace/{id}/context/{cid}/{cid2}")
    fun moveContext(@PathVariable("id") id: Long,
                    @PathVariable("cid") cid1: Long,
                    @PathVariable("cid2") cid2: Long,
                    before: Boolean): Any {
        val currentUser = userService.currentUser() ?: throw InvalidUserException("can't find current user")
        val contextList = languageModelManagementService.moveContext(id, cid1, cid2, before, currentUser)

        return mapOf("contexts" to contextList)
    }

    @ApiOperation("get intent list with cid")
    @ApiKeySecured
    @GetMapping("/workspace/{id}/context/{cid}/intents")
    fun getIntentList(@PathVariable("id") id: Long,
                      @PathVariable("cid") cid: Long): Any {
        val intentList = languageModelManagementService.getIntentList(id, cid)

        return mapOf("intent" to intentList)
    }

    @ApiOperation("set Intents order")
    @ApiKeySecured
    @PostMapping("/workspace/{id}/context/{cid}/intents/order")
    fun setIntentsOrder(@PathVariable("id") id: Long,
                        @PathVariable("cid") cid: Long,
                        @Valid @RequestBody intentIdList: List<Long>): Any {
        val currentUser = userService.currentUser() ?: throw InvalidUserException("can't find current user")
        val intentList = languageModelManagementService.setIntentOrder(id, cid, intentIdList, currentUser)

        return mapOf("intent" to intentList)
    }

    @ApiOperation("move intent from base")
    @ApiKeySecured
    @PutMapping("/workspace/{id}/context/{cid}/intent/{pid}/{ppid}")
    fun moveIntent(@PathVariable("id") id: Long,
                   @PathVariable("cid") cid: Long,
                   @PathVariable("pid") pid: Long,
                   @PathVariable("ppid") ppid: Long,
                   before: Boolean): Any {
        val currentUser = userService.currentUser() ?: throw InvalidUserException("can't find current user")
        val movedIntentList = languageModelManagementService.moveIntent(id, cid, pid, ppid, before, currentUser)

        return mapOf("intent" to movedIntentList)
    }

    @ApiOperation("delete intent")
    @ApiKeySecured
    @DeleteMapping("/workspace/{id}/context/{cid}/intent/{pid}")
    fun deleteIntent(@PathVariable("id") id: Long,
                     @PathVariable("cid") cid: Long,
                     @PathVariable("pid") pid: Long): Any {
        val currentUser = userService.currentUser() ?: throw InvalidUserException("can't find current user")
        val ret = languageModelManagementService.removeIntent(id, cid, pid, currentUser)

        return mapOf("intent" to ret)
    }

    @ApiOperation("create intent")
    @ApiKeySecured
    @PostMapping("/workspace/{id}/context/{cid}/intent")
    fun createIntent(@PathVariable("id") id: Long,
                     @PathVariable("cid") cid: Long,
                     @Valid @RequestBody params: CreateIntentInfo): Any {
        val currentUser = userService.currentUser() ?: throw InvalidUserException("can't find current user")
        val intent = languageModelManagementService.createIntent(id, cid, params, currentUser)

        return mapOf("intent" to intent)
    }

    @ApiKeySecured
    @PutMapping("/workspace/{id}/context/{cid}/intent/{pid}")
    fun modifyIntent(@PathVariable("id") id: Long,
                     @PathVariable("cid") cid: Long,
                     @PathVariable("pid") pid: Long,
                     @Valid @RequestParam params: UpdateIntentInfo): Any {
        val currentUser = userService.currentUser() ?: throw InvalidUserException("can't find current user")
        val intent = languageModelManagementService.modifyIntent(id, cid, pid, params, currentUser)

        return mapOf("intent" to intent)
    }
}