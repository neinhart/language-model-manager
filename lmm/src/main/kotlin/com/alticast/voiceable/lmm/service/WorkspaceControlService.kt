/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.service
      Date  : 19. 3. 15
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.service

import com.alticast.voiceable.lmm.exceptions.InvalidRevisionException
import com.alticast.voiceable.lmm.exceptions.InvalidWorkspaceException
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelRevision
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelWorkspace
import com.alticast.voiceable.lmm.repository.datamodel.User
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.Date

@Service
class WorkspaceControlService {
    private val logger = LoggerFactory.getLogger(WorkspaceControlService::class.java)

    @Autowired
    lateinit var dataService: DataService

    @Autowired
    lateinit var dataManagementService: DataManagementService

    fun getWorkspaceListByIds(idList: List<Long>?): List<LanguageModelWorkspace> {
        return dataService.findWorkspaceListByIds(idList)
    }

    fun getUpdateTime(workspace: LanguageModelWorkspace): Date? {
        val workingRevision = dataService.findWorkingRevision(workspace.id)

        return workingRevision?.updatedAt
    }

    @Transactional
    fun createWorkspace(baseWorkspaceId: Long, baseRevisionId: Long?, name: String?, currentUser: User,
                        filterVariants: String?, filterLanguages: String?): LanguageModelWorkspace {

        val newWorkspace = dataService.save(LanguageModelWorkspace(name = name
                ?: "Unknown", editable = true, owner = currentUser))

        val baseWorkspace = dataService.findWorkspaceById(baseWorkspaceId).orElse(newWorkspace)

        val newRevision = dataService.save(
                LanguageModelRevision(working = true, languageModelWorkspace = newWorkspace,
                        comments = "workspace is created base on workspace(${baseWorkspace.name})"))

        if (baseWorkspace == newWorkspace) {
            logger.debug("can't find base workspace id $baseWorkspaceId --> create empty workspace")
            dataService.save(newRevision)
        } else {
            val baseRevision = dataService.findRevision(baseWorkspace.id, baseRevisionId)
                ?: throw InvalidRevisionException(baseWorkspace.id, baseRevisionId,
                        "Can't find base workspace/revision id - ${baseWorkspace.name} $baseRevisionId")

            dataManagementService.copyRevision(baseRevision, newRevision, filterVariants, filterLanguages)
        }

        return newWorkspace
    }

    @Transactional
    fun deleteWorkspace(workspaceId: Long): Any {
        val workspace = dataService.findWorkspaceById(workspaceId).orElseThrow {
            logger.warn("Can't find workspace - $workspaceId")
            InvalidWorkspaceException(workspaceId, "can't find workspace by id $workspaceId")
        }

        dataService.delete(workspace)

        val revisionList = dataService.findAllRevisionsByWorkspace(workspaceId)
        revisionList?.forEach { revision ->
            dataManagementService.removeRevision(revision)
        }

        return workspace
    }

    fun updateWorkspace(workspaceId: Long, name: String): Any {
        val workspace = dataService.findWorkspaceById(workspaceId).orElseThrow {
            logger.warn("Can't find workspace - $workspaceId")
            throw InvalidWorkspaceException(workspaceId, "can't find workspace by id $workspaceId")
        }
        workspace.name = name

        return dataService.save(workspace)
    }

    fun findAllRevisions(workspaceId: Long): List<LanguageModelRevision>? {
        return dataService.findAllRevisionsByWorkspace(workspaceId)
    }

    fun rollbackRevision(workspaceId: Long, revisionId: Long, user: User): LanguageModelRevision {
        val workspace = dataService.findWorkspaceById(workspaceId).orElseThrow {
            logger.warn("Can't find workspace and revision ($workspaceId/$revisionId)")
            InvalidWorkspaceException(workspaceId, "Can't find revision ($revisionId) in workspace($workspaceId)")
        }
        val srcRevision = dataService.findRevision(workspaceId, revisionId) ?: let {
            logger.warn("Can't find workspace and revision ($workspaceId/$revisionId)")
            throw InvalidRevisionException(workspaceId, revisionId, "Can't find revision ($revisionId) in workspace($workspaceId)")
        }

        val newRevision = dataService.save(
                LanguageModelRevision(working = false, languageModelWorkspace = workspace, comments = "rollback from revision $revisionId", user = user)
        )

        dataManagementService.copyRevision(srcRevision, newRevision, fVariants = null, fLanguages = null)

        logger.debug("copy last revision to work set")
        val workRevision = dataService.findWorkingRevision(workspaceId) ?: let {
            logger.warn("Can't find work set in workspace $workspaceId")
            throw InvalidWorkspaceException(workspaceId, "Can't find working set in workspace")
        }

        dataManagementService.updateRevision(workRevision, newRevision)

        return newRevision
    }

    fun commitWorkset(workspaceId: Long, user: User, message: String?): LanguageModelRevision {

        val workspace = dataService.findWorkspaceById(workspaceId).orElseThrow {
            logger.warn("Can't find workspace - $workspaceId")
            InvalidWorkspaceException(workspaceId, "Can't find workspace($workspaceId)")
        }

        val workingSet = dataService.findWorkingRevision(workspaceId) ?: let {
            logger.warn("Can't find working set in workspace ($workspaceId)")
            throw InvalidWorkspaceException(workspaceId, "can't find working set in workspace $workspaceId")
        }

        val newRevision = dataService.save(
                LanguageModelRevision(working = false, languageModelWorkspace = workspace, comments = message
                        ?: "commit by user(${user.userId})", user = user)
        )

        dataManagementService.copyRevision(workingSet, newRevision, null, null)

        return newRevision
    }

    fun makeDeployRevision(workspaceId: Long, revisionId: Long?, user: User, message: String): LanguageModelRevision {
        val workspace = dataService.findWorkspaceById(workspaceId).orElseThrow {
            logger.warn("Can't find workspace ($workspaceId)")
            InvalidWorkspaceException(workspaceId, "Can't find workspace($workspaceId)")
        }
        val revision = dataService.findRevision(workspaceId, revisionId) ?: let {
            logger.warn("Can't find workspace and revision ($workspaceId/$revisionId)")
            throw InvalidRevisionException(workspaceId, revisionId, "Can't find revision ($revisionId) in workspace($workspaceId)")
        }

        val deployRevision = dataService.save(
                LanguageModelRevision(working = false, languageModelWorkspace = workspace, comments = message, user = user)
        )

        dataManagementService.copyRevision(revision, deployRevision, null, null)
        revision.deployRevisionId = deployRevision.id
        dataService.save(revision)

        return deployRevision
    }
}
