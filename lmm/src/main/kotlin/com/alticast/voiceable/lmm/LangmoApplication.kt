package com.alticast.voiceable.lmm

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableJpaAuditing
@EnableScheduling
class LangmoApplication

fun main(args: Array<String>) {

    runApplication<LangmoApplication>(*args)
}
