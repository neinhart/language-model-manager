/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.repository.datamodel
      Date  : 19. 3. 12
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.repository.datamodel

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.io.File
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class LanguageModelEntity(
        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0,

        var name: String = "",
        var fileDataKey: String = "",
        var headers: String = "",

        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @OnDelete(action = OnDeleteAction.CASCADE)
        @JsonIgnore
        private var languageModel: LanguageModel? = null
) {
    companion object {
        val baseDir = "entities"
        fun createEntityModel(parentModel: LanguageModel, dataKey: String, dataFile: File,
                              onEntity: (model: LanguageModelEntity) -> Unit,
                              onEntityItem: (model: LanguageModelEntity, data: String) -> Unit): LanguageModelEntity {

            val data = dataFile.bufferedReader().use { it.readLines() }
            val header = data.first()
            val entityModel = LanguageModelEntity(fileDataKey = dataKey, languageModel = parentModel, headers = header, name = dataFile.name)

            onEntity(entityModel)

            for (item in data.drop(1)) {
                if (item.startsWith("#") || item.isEmpty() || item.isBlank())
                    continue
                onEntityItem(entityModel, item)
            }

            return entityModel
        }

        fun writeEntityModel(dataDir: File, entityModel: LanguageModelEntity, entityItems: List<LanguageModelEntityItem>) {
            val dstFile = File(dataDir, "$baseDir/${entityModel.name}")
            if (!dstFile.parentFile.isDirectory)
                dstFile.parentFile.mkdirs()

            dstFile.bufferedWriter().use { bw ->
                bw.write(entityModel.headers + "\n")
                entityItems.forEach {
                    bw.write(it.dataItem + "\n")
                }
            }
        }

        fun createLanguageModelEntity(name: String, fileDataKey: String, headers: String, model: LanguageModel): LanguageModelEntity {
            return LanguageModelEntity(name = name, fileDataKey = fileDataKey, headers = headers, languageModel = model)
        }

        fun loadEntityItemList(dataPath: String, entity: LanguageModelEntity): List<String>? {
            if (entity.fileDataKey.isEmpty()) return null

            return File(File(dataPath), entity.fileDataKey).bufferedReader().use { it.readLines().drop(1) }
        }
    }
}