package com.alticast.voiceable.lmm.repository

import com.alticast.voiceable.lmm.repository.datamodel.ActionParam
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ActionParamRepository : JpaRepository<ActionParam, Long> {
    fun findAllByVariant(variant: String): List<ActionParam>
}