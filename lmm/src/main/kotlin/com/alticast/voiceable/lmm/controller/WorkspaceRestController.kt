/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.controller
      Date  : 19. 3. 15
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.controller

import com.alticast.voiceable.lmm.exceptions.InvalidUserException
import com.alticast.voiceable.lmm.jwt.ApiKeySecured
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelWorkspace
import com.alticast.voiceable.lmm.service.DataManagementService
import com.alticast.voiceable.lmm.service.IngestLanguageModel
import com.alticast.voiceable.lmm.service.UserService
import com.alticast.voiceable.lmm.service.WorkspaceControlService
import com.alticast.voiceable.lmm.service.cached.LanguageModelManagementService
import com.fasterxml.jackson.annotation.JsonRootName
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@JsonRootName("workspace")
data class CreateWorkspace(
        val workspaceId: Long? = null,
        val revision: Long? = null,
        val name: String? = null,
        val filterVariants: String? = null,
        val filterLanguage: String? = null
)

@JsonRootName("workspace")
data class UpdateWorkspace(val name: String)

@RestController
class WorkspaceRestController {
    private val logger = LoggerFactory.getLogger(WorkspaceRestController::class.java)

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var workspaceControlService: WorkspaceControlService

    @Autowired
    lateinit var dataManagementService: DataManagementService

    @Autowired
    lateinit var modelManagementService: LanguageModelManagementService

    @Autowired
    lateinit var ingestLanguageModel: IngestLanguageModel

    private fun simplied(ws: LanguageModelWorkspace): Map<String, Any> =
            mapOf(
                    "name" to ws.name,
                    "deploy" to !ws.editable,
                    "id" to ws.id,
                    "updated" to (workspaceControlService.getUpdateTime(ws) ?: "error"),
                    "owner" to mapOf(
                            "id" to ws.owner?.userId,
                            "email" to ws.owner?.email
                    )
            )

    @ApiKeySecured
    @GetMapping("/workspace")
    fun getWorkspaceList(@RequestParam("ids", required = false) idList: List<Long>? = null): Any {
        val workspaces = workspaceControlService.getWorkspaceListByIds(idList)

        return mapOf("workspace" to workspaces.map { simplied(it) })
    }

    @ApiKeySecured
    @PostMapping("/workspace")
    fun createWorkspace(@Valid @RequestBody params: CreateWorkspace): Any {
        val currentUser = userService.currentUser() ?: throw InvalidUserException("can't find current user")

        val baseWorkspaceId = params.workspaceId?:-1

        val newWorkspace = workspaceControlService.createWorkspace(
                baseWorkspaceId, params.revision, params.name, currentUser, params.filterVariants, params.filterLanguage
        )

        userService.updateWorkspace(newWorkspace.id, currentUser)

        return mapOf("workspace" to simplied(newWorkspace))
    }

    @ApiKeySecured
    @DeleteMapping("/workspace/{id}")
    fun deleteWorkspace(@PathVariable("id") id: Long): Any {
        return mapOf("workspace" to workspaceControlService.deleteWorkspace(id))
    }

    @ApiKeySecured
    @PutMapping("/workspace/{id}")
    fun updateWorkspace(@PathVariable("id") id: Long,
                        @RequestBody update: UpdateWorkspace): Any {
        val currentUser = userService.currentUser() ?: throw InvalidUserException("can't find current user")

        userService.updateWorkspace(id, currentUser)

        return mapOf("workspace" to workspaceControlService.updateWorkspace(id, update.name))
    }

    @ApiKeySecured
    @GetMapping("/workspace/{id}/rev/list")
    fun getWorkspaceHistory(@PathVariable("id") id: Long, start: Int?, count: Int?): Any {
        val revionList = workspaceControlService.findAllRevisions(id) ?: listOf()
        val resultList = revionList.drop(start ?: 0).take(count ?: 100).map {
            mapOf(
                    "id" to it.id,
                    "comments" to it.comments,
                    "user" to it.user?.username,
                    "updated" to it.updatedAt,
                    "deploy" to it.deployRevisionId
            )
        }

        return mapOf(
                "info" to mapOf("start" to start, "count" to count, "total" to resultList.count()),
                "history" to resultList
        )
    }


    @ApiKeySecured
    @GetMapping("/workspace/{id}/rev/{revId}/rollback")
    fun getWorkspaceRollback(@PathVariable("id") workspaceId: Long,
                             @PathVariable("revId") rollbackRevId: Long): Any {

        val currentUser = userService.currentUser() ?: throw InvalidUserException("can't find current user")

        userService.updateWorkspace(workspaceId, currentUser)

        return mapOf(
                "history" to workspaceControlService.rollbackRevision(workspaceId, rollbackRevId, currentUser)
        )
    }

    @ApiKeySecured
    @PutMapping("/workspace/{id}/commit")
    fun commitWorkspaceRevision(@PathVariable("id") workspaceId: Long, messages: String?): Any {
        val currentUser = userService.currentUser() ?: throw InvalidUserException("can't find current user")

        userService.updateWorkspace(workspaceId, currentUser)

        return mapOf("history" to workspaceControlService.commitWorkset(workspaceId, currentUser, messages))
    }


    @ApiKeySecured
    @PutMapping("/workspace/{id}/rev/{revId}/deploy")
    fun deployWorkspace(@PathVariable("id") workspaceId: Long,
                        @PathVariable("revId") revId: Long?, messages: String?): Any {
        val currentUser = userService.currentUser() ?: throw InvalidUserException("can't find current user")

        userService.updateWorkspace(workspaceId, currentUser)
        modelManagementService.deployRevision(revId, messages)?.run {
            ingestLanguageModel.ingestLanguageModel(this)
        }

        return mapOf("deploy" to "ok")
    }

    @GetMapping("/profile/list")
    fun getProfileList(): Any {
        //  Variant to [Languages]
        return dataManagementService.getProfileList()
    }

}