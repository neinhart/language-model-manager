/*
    Copyright (C) 2018 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.langmo.entity
      Date  : 18. 9. 18
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.repository.datamodel

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonRootName
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import java.time.LocalDateTime
import javax.persistence.ElementCollection
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
@JsonRootName("user")
data class User(var email: String = "anonymous@alticast.com",
                @JsonIgnore
                var password: String = "",
                var token: String = "",
                var username: String = "AutoGenUser",
                var userId: String = "testuser",
                var role: String = "admin",

                @ElementCollection
                val recently: MutableList<Long> = mutableListOf(),

                @CreatedDate
                val regDate: LocalDateTime = LocalDateTime.now(),
                @LastModifiedDate
                var updateDate: LocalDateTime = LocalDateTime.now(),
                @Id @GeneratedValue(strategy = GenerationType.AUTO)
                var id: Long = 0) {
    override fun toString(): String = "User($email, $username)"
}
