/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.repository.datamodel
      Date  : 19. 3. 12
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.repository.datamodel

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class LanguageModel(
        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0,

        var variant: String = "",
        var language: String = "",
        var appName: String = "",

        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @OnDelete(action = OnDeleteAction.CASCADE)
        @JsonIgnore
        private var languageModelRevision: LanguageModelRevision? = null,

        @CreatedDate
        val regDate: LocalDateTime = LocalDateTime.now(),
        @LastModifiedDate
        val updateDate: LocalDateTime = LocalDateTime.now()
) {
    companion object {
        fun createLanguageModel(variant: String, language: String, appName: String, baseRevision: LanguageModelRevision): LanguageModel {
            return LanguageModel(variant = variant, language = language, appName = appName, languageModelRevision = baseRevision)
        }
    }
}