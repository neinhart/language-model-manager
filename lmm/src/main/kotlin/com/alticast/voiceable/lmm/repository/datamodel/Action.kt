package com.alticast.voiceable.lmm.repository.datamodel

import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import javax.persistence.AttributeConverter
import javax.persistence.Convert
import javax.persistence.Converter
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Lob

data class ActionParamInfo(val name: String, val optional: Boolean?, val type: String)
data class ActionParamGroupInfo(val name: String, val optional: Boolean?, val type: String, val params: List<ActionParamInfo>?)
data class ActionParamDto(val name: String, val script: String, val help: String?, val group: ActionParamGroupInfo?, val params: List<ActionParamInfo>?)
data class ActionParamIntentsDto(val variant: String?, var intents: MutableList<ActionParamDto>? = mutableListOf())

data class ActionParamInfoData(val group: ActionParamGroupInfo?, val params: List<ActionParamInfo>?)

@Converter
class ActionParamInfoDataConverter : AttributeConverter<ActionParamInfoData, String> {
    override fun convertToDatabaseColumn(attribute: ActionParamInfoData?): String? {
        return ActionParam.toJson(attribute)
    }

    override fun convertToEntityAttribute(dbData: String?): ActionParamInfoData? {
        return ActionParam.fromJson(dbData)
    }
}

@Entity
data class ActionParam(
        val name: String = "",
        val variant: String = "",
        val script: String = "",
        val help: String = "",

        @Lob
        @Convert(converter = ActionParamInfoDataConverter::class)
        val params: ActionParamInfoData? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null
) {
    companion object {
        private val objectMapper = ObjectMapper().registerModule(KotlinModule())
        fun toJson(data: ActionParamInfoData?): String? {
            data ?: return null

            return objectMapper.writeValueAsString(data)
        }

        fun fromJson(data: String?): ActionParamInfoData? {
            data ?: return null
            try {
                val jsonParser = JsonFactory().createParser(data)

                return objectMapper.readValue(jsonParser)
            } catch (ex: Exception) {
                return null
            }
        }
    }
}