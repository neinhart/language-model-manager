/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.service
      Date  : 19. 4. 1
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.service

import com.alticast.text.dictionary.DictionaryManager
import com.alticast.text.entity.EntityManager
import com.alticast.text.entity.EntityParser
import com.alticast.text.entity.GenericEntityValue
import com.alticast.text.entity.SubEntityData
import com.alticast.text.entity.SubEntityMatcher
import com.alticast.text.entity.SubEntityUserData
import com.alticast.text.entity.SystemEntityManager
import com.alticast.text.pattern.MultiGraphPatternManager
import com.alticast.text.pattern.PatternMatcher
import com.alticast.text.pattern.PatternResult
import com.alticast.voiceable.lmm.exceptions.InvalidLanguageModelException
import com.alticast.voiceable.lmm.service.cached.CachedLanguageModelDictInfo
import com.alticast.voiceable.lmm.service.cached.LanguageModelDictId
import com.alticast.voiceable.lmm.service.cached.LanguageModelEntityId
import com.alticast.voiceable.lmm.service.cached.LanguageModelManagementService
import com.google.common.cache.CacheBuilder
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

data class EntityFinderResult(val appName: String, val dataName: String, val total: Int, val fields: List<String>, val dataList: List<List<String>>)

data class AppMultiGraphManager(val appName: String, val multiMatcher: MultiGraphPatternManager)

data class FoundPhraseResult(val appName: String, val found: PatternResult?, val alternatives: List<PatternResult>)
@Service
class MatcherService {
    private val logger = LoggerFactory.getLogger(MatcherService::class.java)

    @Autowired
    lateinit var cachedService: LanguageModelManagementService

    @Autowired
    lateinit var dataService: DataService

    @Value("\${lmm.matcher-cache}")
    private val matcherCacheSize: Long = 16

    private val matcherCache = CacheBuilder.newBuilder().maximumSize(matcherCacheSize)
            .build<String, List<AppMultiGraphManager>>()

    private fun findMatcherList(wsId: Long, revId: Long?, variant: String, language: String): List<AppMultiGraphManager> {
        val matcherKey = "matcher.$wsId.$revId.$variant.$language"

        val matcherList = matcherCache.get(matcherKey) {
            logger.debug("Cache missed - generate matchers with $matcherKey")
            val multiGraphManagerList = mutableListOf<AppMultiGraphManager>()
            val revision = dataService.findRevision(wsId, revId)
                    ?: throw InvalidLanguageModelException("can't find revision with ($wsId/$revId/$variant/$language)")

            val modelList = dataService.findLanguageModelByIdVariantLanguage(revision.id, variant, language)
                    ?: throw InvalidLanguageModelException("can't find language models with ($wsId/$revId/$variant/$language)")

            val contextList = cachedService.findAllContextByRevision(wsId, revision.id)
            for (model in modelList) {
                logger.debug(" - generate $model")
                val dictList = dataService.findAllDictionariesByLanguageModel(model.id)?.map {
                    val dictId = LanguageModelDictId(wsId, revId, variant, language, model.appName,
                            CachedLanguageModelDictInfo.getDictType(it.headers),
                            CachedLanguageModelDictInfo.getDictName(it.headers))
                    cachedService.findLanguageModelDictionary(dictId)
                } ?: listOf()

                val entityList = dataService.findAllEntitiesByLanguageModel(model.id)?.map {
                    val entityId = LanguageModelEntityId(wsId, revId, variant, language, model.appName, it.name)
                    cachedService.findLanguageModelEntity(entityId)
                } ?: listOf()

                val dictManager = DictionaryManager()
                dictList.forEach {
                    dictManager.addDictionary(it.dictType, it.dictName, it.dictSubName,
                            it.itemList.map { dictItemInfo ->
                                it.getDictItemDbData(dictItemInfo)
                            })
                }

                val entityManager = EntityManager()
                entityList.forEach {
                    val entityMatcher = SubEntityMatcher(it.name, dictManager)
                    val fieldsMap = mutableMapOf<String, Int>()

                    fieldsMap.putAll(it.fields.drop(1).mapIndexed { index, ss -> ss.toLowerCase() to index })

                    val entityDataList = it.itemList.map {
                        SubEntityData.createObject(it.data, fieldsMap)
                    }
                    SubEntityData.generateEntityPatterns(entityDataList, dictManager, it.name)

                    entityMatcher.addEntities(entityDataList)
                    entityManager.addParser(
                            EntityParser(it.name, entityMatcher)
                    )
                }

                //  VS와는 달리 LMM는 Pattern Match정도를 체크하므로, app단위로 MultiMatcher하나만 가져간다
                val appContext = contextList.filter {
                    it.variant == model.variant && it.language == model.language && it.appName == model.appName
                }

                val basePriority = appContext.minBy { it.priority }?.priority ?: 100
                val multiMatcher = MultiGraphPatternManager(model.appName, basePriority, entityManager = entityManager, dictionaryManager = dictManager)
                appContext.forEach { context ->
                    cachedService.findAllIntentListByContext(context.id).forEach { intent ->
                        val matchClass = when (intent.matchClass.toLowerCase()) {
                            "immediate" -> PatternMatcher.MatchClass.IMMEDIATE_CLASS
                            else -> PatternMatcher.MatchClass.NORMAL_CLASS
                        }
                        intent.patterns?.forEach { pattern ->
                            multiMatcher.addPatternMatcher(null,
                                    PatternMatcher(intent.name, matchClass, pattern, intent.action),
                                    intent.priority)
                        }
                    }
                }

                multiGraphManagerList.add(AppMultiGraphManager(model.appName, multiMatcher))
            }

            multiGraphManagerList
        }

        return matcherList
    }

    fun invalidateCache() {
        matcherCache.invalidateAll()
    }

    fun findEntity(wsId: Long, revId: Long?, variant: String, language: String,
                   appName: String?, dataName: String?, queryString: String): List<EntityFinderResult> {
        val foundList = mutableListOf<EntityFinderResult>()

        val matcherList = findMatcherList(wsId, revId, variant, language)

        matcherList.forEach { appManager ->
            val entityManager =
                    if ((appName == null) || (appName == appManager.appName))
                        appManager.multiMatcher.getEntityManager()
                    else null

            if (entityManager != null) {
                val groupList = entityManager.getGroups().filter { it == (dataName ?: it) }
                for (group in groupList) {
                    if (!SystemEntityManager.contains(group)) {
                        val matcher = entityManager.getMatcher(group)
                        val result = when (matcher) {
                            null -> null
                            is SubEntityMatcher -> {
                                matcher.findWithHitEnd(queryString)
                            }
                            else -> matcher.find(queryString)
                        }?.filter { it.end - it.start == queryString.length }?.sortedByDescending { it.confidence }

                        if (result != null && result.isNotEmpty()) {
                            val entityValue = result.first().value
                            val userData = entityValue.user

                            if (userData is SubEntityUserData) {
                                val fields = listOf("speech") + userData.fields!!.toList().sortedBy { (_, value) -> value }.map { it.first }
                                val values = result.map {
                                    val value = it.value as GenericEntityValue
                                    val eData = value.user as SubEntityUserData

                                    listOf(value.orgName) + eData.values
                                }.take(20)

                                foundList.add(EntityFinderResult(appManager.appName, group, result.count(), fields, values))
                            }
                        }
                    }
                }
            }
        }


        return foundList
    }

    fun findPhrase(wsId: Long, revId: Long?, variant: String, language: String, appName: String?, phrase: String): List<FoundPhraseResult> {
        val phraseResult = mutableListOf<FoundPhraseResult>()
        val matcherList = findMatcherList(wsId, revId, variant, language)

        for (appManager in matcherList) {
            if (appName == null || appName == appManager.appName) {
                val matcher = appManager.multiMatcher
                val result = matcher.match(phrase)
                phraseResult.add(FoundPhraseResult(appManager.appName, result.first, result.second))
            }
        }

        return phraseResult
    }
}

