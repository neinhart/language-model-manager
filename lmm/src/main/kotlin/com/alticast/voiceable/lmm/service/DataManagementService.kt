/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.service
      Date  : 19. 3. 15
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.service

import com.alticast.voiceable.lmm.repository.datamodel.LanguageModel
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelDictionary
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelDictionaryItem
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelEntity
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelEntityItem
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelPrivate
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelRevision
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelStory
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelStoryItem
import com.alticast.voiceable.lmm.repository.datamodel.User
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DataManagementService {
    private val logger = LoggerFactory.getLogger(DataManagementService::class.java)

    @Autowired
    lateinit var dataService: DataService

    fun copyRevision(src: LanguageModelRevision, dst: LanguageModelRevision, fVariants: String?, fLanguages: String?) {
        logger.debug("copy revision - ${src.id} to ${dst.id}")

        dataService.findLanguageModelListByRevisionId(src.id)?.forEach { srcModel ->
            val contains =
                    (fVariants?.contains(srcModel.variant) ?: true) && (fLanguages?.contains(srcModel.language) ?: true)

            if (contains) {
                val dstModel = dataService.save(
                        LanguageModel.createLanguageModel(srcModel.variant, srcModel.language, srcModel.appName, dst))

                copyLanguageModel(srcModel, dstModel)
            }
        }
    }

    fun copyLanguageModel(src: LanguageModel, dst: LanguageModel) {
        logger.debug("copy language model - ${src.id} to ${dst.id}")

        dataService.findAllEntitiesByLanguageModel(src.id)?.forEach { srcEntity ->
            val dstEntity = dataService.save(
                    LanguageModelEntity.createLanguageModelEntity(srcEntity.name, srcEntity.fileDataKey, srcEntity.headers, dst)
            )

            copyLanguageModelEntity(srcEntity, dstEntity)
        }

        dataService.findAllDictionariesByLanguageModel(src.id)?.forEach { srcDict ->
            val dstDict = dataService.save(
                    LanguageModelDictionary.createLanguageModelDict(srcDict.name, srcDict.fileDataKey, srcDict.headers, dst)
            )

            copyLanguageModelDict(srcDict, dstDict)
        }

        dataService.findAllPrivatesByLanguageModel(src.id)?.forEach {
            dataService.save(
                    LanguageModelPrivate.createLanguageModelPrivate(it.name, it.fileDataKey, it.data, dst)
            )
        }

        dataService.findAllStoriesByLanguageModel(src.id)?.forEach { srcStory ->
            val dstModel = dataService.save(
                    LanguageModelStory.createLanguageModelStory(srcStory.name, srcStory.header, dst)
            )

            copyLanguageModelStory(srcStory, dstModel)
        }
    }

    fun copyLanguageModelEntity(src: LanguageModelEntity, dst: LanguageModelEntity) {
        logger.debug("copy entities - ${src.id} to ${dst.id}")

        dataService.findAllEntityItemsByEntity(src.id)?.forEach {
            dataService.save(LanguageModelEntityItem.createEntityItem(it.dataItem, dst))
        }
    }

    fun copyLanguageModelDict(src: LanguageModelDictionary, dst: LanguageModelDictionary) {
        logger.debug("copy dictionaries - ${src.id} to ${dst.id}")

        dataService.findAllDictionaryItemsByDictionary(src.id)?.forEach {
            dataService.save(LanguageModelDictionaryItem.createLanguageModelDictionaryItem(it.dataItem, model = dst))
        }
    }

    fun copyLanguageModelStory(src: LanguageModelStory, dst: LanguageModelStory) {
        logger.debug("copy stories - ${src.id} to ${dst.id}")
        dataService.findAllStoryItemsByStory(src.id)?.forEach {
            dataService.save(LanguageModelStoryItem.createLanguageModelStoryItem(it.intent, dst))
        }
    }

    fun removeRevision(rev: LanguageModelRevision) {
        logger.debug("remove Revision - ${rev.id} (${rev.comments})")
        dataService.findLanguageModelListByRevisionId(rev.id)?.forEach { model ->
            removeModel(model)
            dataService.delete(model)
        }
    }

    fun removeModel(model: LanguageModel) {
        logger.debug("remove Model - ${model.id} (${model.variant}/${model.language}/${model.appName})")

        dataService.findAllStoriesByLanguageModel(model.id)?.forEach { story ->
            removeStory(story)
            dataService.delete(story)
        }

        dataService.findAllDictionariesByLanguageModel(model.id)?.forEach { dict ->
            removeDictionary(dict)
            dataService.delete(dict)
        }

        dataService.findAllEntitiesByLanguageModel(model.id)?.forEach { entity ->
            removeEntity(entity)
            dataService.delete(entity)
        }

        dataService.findAllPrivatesByLanguageModel(model.id)?.forEach { private ->
            dataService.delete(private)
        }
    }

    fun removeStory(story: LanguageModelStory) {
        dataService.findAllStoryItemsByStory(story.id)?.forEach {
            dataService.delete(it)
        }
    }

    fun removeDictionary(dict: LanguageModelDictionary) {
        dataService.findAllDictionaryItemsByDictionary(dict.id)?.forEach {
            dataService.delete(it)
        }
    }

    fun removeEntity(entity: LanguageModelEntity) {
        dataService.findAllEntityItemsByEntity(entity.id)?.forEach {
            dataService.delete(it)
        }
    }

    //  DST의 내용을 삭제하고 src내용을 복사한다
    fun updateRevision(dst: LanguageModelRevision, src: LanguageModelRevision): LanguageModelRevision {
        dataService.findLanguageModelListByRevisionId(dst.id)?.forEach { model ->
            removeModel(model)
        }

        copyRevision(src, dst, null, null)

        return dst
    }

    fun getProfileList(): Map<String, Set<String>> {
        val resultMap = mutableMapOf<String, Set<String>>()
        val variantGroup = dataService.findAllLanguageModels().map {
            Pair(it.variant, it.language)
        }.groupBy {
            it.first
        }.forEach { t, u ->
            resultMap[t] = u.map { it.second }.toSet()
        }

        return resultMap
    }

    fun findStoriesLockBy(user: User): List<LanguageModelStory> {
        return dataService.findAllStoriesByUser(user.id) ?: listOf()
    }
}