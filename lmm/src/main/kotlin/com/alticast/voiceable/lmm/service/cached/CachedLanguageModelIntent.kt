/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.service.cached
      Date  : 19. 3. 29
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.service.cached

import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelStory
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelStoryItem
import com.alticast.voiceable.lmm.service.ActionParamIntents
import com.alticast.voiceable.lmm.service.ActionParamService
import com.fasterxml.jackson.annotation.JsonIgnore

class CachedLanguageModelIntent(
        @JsonIgnore val story: LanguageModelStory,
        @JsonIgnore val storyItem: LanguageModelStoryItem) {
    var patterns = storyItem.intent.patterns
    var action = storyItem.intent.action
    var alias = storyItem.intent.alias
    var matchClass = storyItem.intent.matchClass
    var hints = storyItem.intent.hints
    var close = storyItem.intent.close
    var name = storyItem.intent.name
    var priority = storyItem.intent.priority
    var id = storyItem.id
    var actionParam = generateActionParams(storyItem.intent.action)

    fun updateData() {
        storyItem.intent.let {
            it.patterns = patterns
            it.action = actionParam?.let { generateAction(it) } ?: action
            it.alias = alias
            it.matchClass = matchClass
            it.hints = hints
            it.close = close
            it.name = name
            it.priority = priority
        }
    }

    companion object {
        fun generateActionParams(action: String): ActionParamIntents? {
            return ActionParamService.getActionParamFromAction(action)
        }

        fun generateAction(actionParams: ActionParamIntents): String {
            return ActionParamService.getActionFromActionParam(actionParams)
        }
    }

}