/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.repository.datamodel
      Date  : 19. 3. 12
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.repository.datamodel

import com.alticast.voiceable.lmm.utils.StoryIntent
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import javax.persistence.AttributeConverter
import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Lob
import javax.persistence.ManyToOne

data class LanguageModelStoryItemIntent(
        var patterns: List<String>? = null,
        var action: String = "",
        var alias: String = "",
        var matchClass: String = "",
        var hints: List<String> = listOf(),
        var close: Boolean = false,
        var name: String = "",
        var priority: Int = 0
) {
    companion object {
        fun convertStoryIntentToLanguageModelIntent(intent: StoryIntent): LanguageModelStoryItemIntent {
            val patterns = mutableListOf<String>()

            intent.pattern?.run { patterns.add(this) }
            intent.patterns?.run { patterns.addAll(this) }

            return LanguageModelStoryItemIntent(
                    patterns = patterns,
                    action = intent.action ?: "",
                    alias = intent.alias ?: "",
                    matchClass = intent.matchClass ?: "",
                    hints = intent.hints ?: listOf(),
                    close = intent.close ?: false,
                    name = intent.name ?: "",
                    priority = intent.priority ?: 0
            )
        }

        fun convertLanguageModelIntentToStoryIntent(idx: Int, intent: LanguageModelStoryItemIntent): StoryIntent {
            return StoryIntent(
                    id = idx,
                    pattern = null, patterns = intent.patterns,
                    action = intent.action, matchClass = intent.matchClass,
                    hints = intent.hints, close = intent.close,
                    name = intent.name, priority = intent.priority,
                    alias = intent.alias)
        }
    }
}

@Convert
class StoryItemIntentConverter : AttributeConverter<LanguageModelStoryItemIntent, String> {
    override fun convertToDatabaseColumn(attribute: LanguageModelStoryItemIntent?): String {
        return ObjectMapper().writeValueAsString(attribute)
    }

    override fun convertToEntityAttribute(dbData: String): LanguageModelStoryItemIntent {
        return ObjectMapper().readValue(dbData)
    }
}

@Entity
data class LanguageModelStoryItem(
        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0,

        @Lob
        @Convert(converter = StoryItemIntentConverter::class)
        val intent: LanguageModelStoryItemIntent = LanguageModelStoryItemIntent(),

        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @OnDelete(action = OnDeleteAction.CASCADE)
        @JsonIgnore
        var languageModelStory: LanguageModelStory? = null
) {
    companion object {
        fun createLanguageModelStoryItem(intent: LanguageModelStoryItemIntent, model: LanguageModelStory): LanguageModelStoryItem {
            return LanguageModelStoryItem(intent = intent, languageModelStory = model)
        }
    }
}
