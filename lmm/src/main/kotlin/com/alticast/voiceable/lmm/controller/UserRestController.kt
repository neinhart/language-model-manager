/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.controller
      Date  : 19. 3. 18
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.controller

import com.alticast.voiceable.lmm.exceptions.InvalidUserException
import com.alticast.voiceable.lmm.jwt.ApiKeySecured
import com.alticast.voiceable.lmm.service.DataManagementService
import com.alticast.voiceable.lmm.service.UserService
import com.fasterxml.jackson.annotation.JsonRootName
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

@JsonRootName("user")
data class LogIn(val userid: String, val password: String)

@JsonRootName("user")
data class Register(@NotNull(message = "can't be missing") @Size(min = 1, message = "can't be empty") @Pattern(regexp = "^\\w+$", message = "must be alphanumeric")
                    val userId: String,
                    @NotNull(message = "can't be missing") @Size(min = 1, message = "can't be empty")
                    val username: String,
                    var role: String?,
                    @NotNull(message = "can't be missing") @Size(min = 1, message = "can't be empty") @Pattern(regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$", message = "must be a valid email")
                    var email: String,
                    @NotNull(message = "can't be missing") @Size(min = 1, message = "can't be empty")
                    var password: String)

@JsonRootName("user")
data class Update(@Size(min = 1, message = "can't be empty") @Pattern(regexp = "^\\w+$", message = "must be alphanumeric")
                  val userid: String,
                  val username: String?,
                  val role: String?,
                  @Size(min = 1, message = "can't be empty") @Pattern(regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$", message = "must be a valid email")
                  val email: String?,
                  val password: String?)

@RestController
class UserRestController {
    private val logger = LoggerFactory.getLogger(UserRestController::class.java)

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var dataManagementService: DataManagementService

    @PostMapping("/session/login")
    fun login(@Valid @RequestBody login: LogIn): Any {
        val user = userService.login(login.userid, login.password) ?: throw InvalidUserException("login failed")

        return mapOf("user" to user)
    }

    @ApiKeySecured
    @PostMapping("/user")
    fun registerUser(@Valid @RequestBody register: Register): Any {
        if (userService.existsUser(register.userId, register.email)) {
            throw InvalidUserException("user ${register.userId} / ${register.email} is already taken")
        }

        return mapOf("user" to
                userService.registerUser(register.userId, register.email, register.username, register.password, register.role)
        )
    }

    @ApiKeySecured
    @PutMapping("/user/{userId}")
    fun updateUser(@PathVariable("userId") userid: String,
                   @RequestBody user: Update): Any {

        return mapOf("user" to
                userService.updateUserInfo(user.userid, user.email, user.username, user.password, user.role))
    }

    @ApiKeySecured
    @DeleteMapping("/user/{userId}")
    fun deleteUser(@PathVariable("userId")
                   userId: String): Any {
        val user = userService.findByUserId(userId) ?: throw InvalidUserException("Can't find user id - $userId")

        userService.removeUser(user)

        return mapOf("user" to mapOf("id" to user.userId, "name" to user.username))
    }

    @ApiKeySecured
    @GetMapping("/user/{userId}/lockby")
    fun getContextLockBy(@PathVariable("userId")
                         userId: String): Any {
        val user = userService.findByUserId(userId) ?: throw InvalidUserException("Can't find user id - $userId")
        val stories = dataManagementService.findStoriesLockBy(user)

        return mapOf("contexts" to stories)
    }

    @ApiKeySecured
    @GetMapping("/user")
    fun currentUser(): Any {
        return mapOf("user" to userService.currentUser())
    }

    @ApiKeySecured
    @GetMapping("/users")
    fun getUsers(): Any {
        return userService.findAllUser()
    }
}