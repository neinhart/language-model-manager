/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.service
      Date  : 19. 3. 19
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.service.cached

import com.alticast.voiceable.lmm.exceptions.InvalidLanguageModelContextException
import com.alticast.voiceable.lmm.exceptions.InvalidLanguageModelException
import com.alticast.voiceable.lmm.exceptions.InvalidRevisionException
import com.alticast.voiceable.lmm.exceptions.InvalidUserException
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModel
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelDictionary
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelDictionaryItem
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelEntity
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelEntityItem
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelRevision
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelStory
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelStoryHeader
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelStoryItem
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelWorkspace
import com.alticast.voiceable.lmm.repository.datamodel.User
import com.alticast.voiceable.lmm.service.ActionParamIntents
import com.alticast.voiceable.lmm.service.ActionParamService
import com.alticast.voiceable.lmm.service.DataManagementService
import com.alticast.voiceable.lmm.service.DataService
import com.alticast.voiceable.lmm.service.UserService
import com.alticast.voiceable.lmm.service.exchange.LanguageModelExchanger
import com.fasterxml.jackson.annotation.JsonRootName
import com.google.common.cache.CacheBuilder
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.io.File

enum class DataUpdateMode {
    update,
    insert,
    delete,
    asitwas
}

open class LanguageModelId(val workspaceId: Long, val revisionId: Long?, val variant: String, val language: String, val appName: String) {
    open fun cacheKey() = "$workspaceId.$revisionId.$variant.$language.$appName"

    override fun toString(): String = "$workspaceId/$revisionId/$variant/$language/$appName"
}

class LanguageModelEntityId(workspaceId: Long, revisionId: Long?, variant: String, language: String, appName: String, val dataName: String) :
        LanguageModelId(workspaceId, revisionId, variant, language, appName) {

    override fun cacheKey() = super.cacheKey() + ".entity.$dataName"
    override fun toString(): String = "LanguageModelEntityId(${super.toString()}.$dataName)"
}

class LanguageModelDictId(workspaceId: Long, revisionId: Long?, variant: String, language: String, appName: String, val dictType: String, val dictName: String) :
        LanguageModelId(workspaceId, revisionId, variant, language, appName) {

    override fun cacheKey() = super.cacheKey() + ".dict.$dictType.$dictName"
    override fun toString(): String = "LanguageModelDictId(${super.toString()}.$dictType.$dictName)"
}

class LanguageModelPrivateId(workspaceId: Long, revisionId: Long?, variant: String, language: String, appName: String, val dataName: String) :
        LanguageModelId(workspaceId, revisionId, variant, language, appName) {

    override fun cacheKey() = super.cacheKey() + ".$dataName"
    override fun toString(): String = "LanguageModelPrivateId(${super.toString()}.$dataName)"
}

data class ContextOrderInfo(val appName: String, val id: Long)

@JsonRootName("context")
data class CreateContext(
        var contextType: String? = null, var contextName: String, var appName: String? = null,
        val variant: String, val language: String
)

@JsonRootName("context")
data class ModifyContext(
        var contextType: String? = null, var contextName: String? = null, var appName: String? = null,
        val variant: String? = null, val language: String? = null)

@JsonRootName("intent")
data class CreateIntentInfo(val name: String = "", val pattern: String = "", val actionName: String = "",
                            val actionParams: List<String>? = null, val hints: List<String>? = null, val matchClass: String = "")

@JsonRootName("intent")
data class UpdateIntentInfo(val name: String? = null, val pattern: String? = null, val actionName: String? = null,
                            val actionParams: List<String>? = null, val hints: List<String>? = null, val matchClass: String? = null)

@Service
class LanguageModelManagementService {
    private val logger = LoggerFactory.getLogger(LanguageModelManagementService::class.java)

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var dataService: DataService

    @Autowired
    lateinit var dataManagementService: DataManagementService

    @Autowired
    lateinit var modelExchanger: LanguageModelExchanger

    @Value("\${lmm.entity.path}")
    private val dataPath: String = ""

    @Value("\${lmm.model-cache}")
    private val cacheModelSize: Long = 128

    private val cachedModel = CacheBuilder.newBuilder().maximumSize(cacheModelSize)
            .build<String, CachedLanguageModelItem>()

    fun invalidateCache() {
        cachedModel.invalidateAll()
    }

    fun findLanguageModelEntityDb(modelId: LanguageModelEntityId): LanguageModelEntity {
        val revision = dataService.findRevision(modelId.workspaceId, modelId.revisionId)
                ?: throw InvalidLanguageModelException("Can't find revision ($modelId)")

        val model = dataService.findLanguageModelByIdVariantLanguageAppName(revision.id, modelId.variant, modelId.language, modelId.appName)
                ?: throw InvalidLanguageModelException("Can't find language model with ($modelId)")

        val dataName = if (modelId.dataName.endsWith(".entity")) modelId.dataName else modelId.dataName + ".entity"

        return dataService.findAllEntitiesByLanguageModelAndDataName(model.id, dataName)?.first()
                ?: throw InvalidLanguageModelException("Can't find entity with ($modelId)")
    }

    fun findLanguageModelDictionaryDb(modelId: LanguageModelDictId): LanguageModelDictionary {
        val model = findLanguageModelModelId(modelId)

        val dict = dataService.findAllDictionariesByLanguageModel(model.id)?.filter {
            val dictType = CachedLanguageModelDictInfo.getDictType(it.headers)
            val dictName = CachedLanguageModelDictInfo.getDictName(it.headers)

            (dictType.toLowerCase() == modelId.dictType.toLowerCase()) && (dictName.toLowerCase() == modelId.dictName.toLowerCase())
        }?.first() ?: throw InvalidLanguageModelException("Can't find dictionary with $modelId")

        return dict
    }

    fun findLanguageModelEntity(modelId: LanguageModelEntityId): CachedLanguageModelItemEntityInfo {
        val languageModelEntity = cachedModel.get(modelId.cacheKey()) {
            val entity = findLanguageModelEntityDb(modelId)

            generateCachedLanguageModelEntityItem(entity)
        }

        return languageModelEntity as CachedLanguageModelItemEntityInfo
    }

    fun findLanguageModelEntity(entityId: Long): CachedLanguageModelItemEntityInfo? {
        val entity = dataService.findEntityById(entityId) ?: return null

        return generateCachedLanguageModelEntityItem(entity)
    }

    private fun findLanguageModelModelId(modelId: LanguageModelId): LanguageModel {
        val revision = dataService.findRevision(modelId.workspaceId, modelId.revisionId)
                ?: throw InvalidLanguageModelException("Can't find revision ($modelId)")

        return dataService.findLanguageModelByIdVariantLanguageAppName(revision.id, modelId.variant, modelId.language, modelId.appName)
                ?: throw InvalidLanguageModelException("Can't find language model with ($modelId)")
    }

    fun findLanguageModelEntityGroupList(modelId: LanguageModelId): List<String> {
        val model = findLanguageModelModelId(modelId)

        return dataService.findAllEntitiesByLanguageModel(model.id)?.map {
            if (it.name.endsWith(".entity")) it.name.dropLast(7) else it.name
        } ?: listOf()
    }

    fun findLanguageModelDictionaryGroupList(modelId: LanguageModelId): Map<String, List<String>> {
        val model = findLanguageModelModelId(modelId)

        val result = mutableMapOf<String, MutableList<String>>()

        dataService.findAllDictionariesByLanguageModel(model.id)?.forEach {
            val dictType = CachedLanguageModelDictInfo.getDictType(it.headers)
            val dictName = CachedLanguageModelDictInfo.getDictName(it.headers)

            result.getOrPut(dictType) {
                mutableListOf()
            }.add(if (dictName.isEmpty()) "global" else dictName)
        }

        return result
    }

    fun findLanguageModelDictionary(modelId: LanguageModelDictId): CachedLanguageModelDictInfo {
        val languageModelDictionary = cachedModel.get(modelId.cacheKey()) {
            val model = findLanguageModelModelId(modelId)

            val dict = dataService.findAllDictionariesByLanguageModel(model.id)?.filter {
                val dictType = CachedLanguageModelDictInfo.getDictType(it.headers)
                val dictName = CachedLanguageModelDictInfo.getDictName(it.headers)

                (dictType.toLowerCase() == modelId.dictType.toLowerCase()) && (dictName.toLowerCase() == modelId.dictName.toLowerCase())
            }?.first() ?: throw InvalidLanguageModelException("Can't find dictionary with $modelId")

            val cachedDictInfo = CachedLanguageModelDictInfo(dict)
            LanguageModelDictionary.loadDictionaryItemList(dataPath, dict)?.let {
                it.forEach {
                    cachedDictInfo.addItem(-1, it)
                }
            } ?: dataService.findAllDictionaryItemsByDictionary(dict.id)?.let {
                it.forEach {
                    cachedDictInfo.addItem(it.id, it.dataItem)
                }
            }

            cachedDictInfo
        }

        return languageModelDictionary as CachedLanguageModelDictInfo
    }

    fun findLanguageModelDictionary(dictId: Long): CachedLanguageModelDictInfo? {
        val dict = dataService.findDictionaryById(dictId) ?: return null

        val cachedDictInfo = CachedLanguageModelDictInfo(dict)
        LanguageModelDictionary.loadDictionaryItemList(dataPath, dict)?.let {
            it.forEach {
                cachedDictInfo.addItem(-1, it)
            }
        } ?: dataService.findAllDictionaryItemsByDictionary(dict.id)?.let {
            it.forEach {
                cachedDictInfo.addItem(it.id, it.dataItem)
            }
        }

        return cachedDictInfo
    }

    private fun generateCachedLanguageModelEntityItem(entity: LanguageModelEntity): CachedLanguageModelItemEntityInfo {
        val cachedEntityInfo = CachedLanguageModelItemEntityInfo(entity)
        LanguageModelEntity.loadEntityItemList(dataPath, entity)?.let {
            it.forEach {
                cachedEntityInfo.addItem(-1, it)
            }
        } ?: dataService.findAllEntityItemsByEntity(entity.id)?.let {
            it.forEach {
                cachedEntityInfo.addItem(it.id, it.dataItem)
            }
        }

        return cachedEntityInfo
    }

    //  fields -->
    fun updateLanguageModelEntities(modelId: LanguageModelEntityId, mode: DataUpdateMode, fields: List<String>, lines: Map<Int, List<String>>) {
        val modelEntityInfo = findLanguageModelEntity(modelId)

        modelEntityInfo.fields.clear()
        modelEntityInfo.fields.addAll(modelEntityInfo.fields.take(1) + fields)

        when (mode) {
            DataUpdateMode.insert -> {
                lines.keys.sortedDescending().forEach {
                    lines[it]?.run {
                        modelEntityInfo.addItem(it, this)//itemList.add(it, this)
                    }
                }
            }
            DataUpdateMode.update -> {
                lines.forEach { t, u ->
                    modelEntityInfo.updateItem(t, u)
                    if (t < modelEntityInfo.itemList.count())
                        modelEntityInfo.updateItem(t, u)//itemList[t] = u
                }
            }
            DataUpdateMode.delete -> {
                lines.forEach { t, _ ->
                    modelEntityInfo.itemList[t].state = DataUpdateMode.delete
                }
            }
            else -> {
            }
        }

        updateCachedLanguageModelEntityItem(modelEntityInfo)
    }

    fun updateLanguageModelDictionaryItems(modelId: LanguageModelDictId, sourceFields: String?, mode: DataUpdateMode, dataList: Map<Int, List<String>>) {
        val modelDictInfo = findLanguageModelDictionary(modelId)
        modelDictInfo.dictType = modelId.dictType
        modelDictInfo.dictName = modelId.dictName
        sourceFields?.run {
            modelDictInfo.fields = modelDictInfo.fields.dropLast(1) + listOf(sourceFields)
        }

        when (mode) {
            DataUpdateMode.insert -> {
                dataList.keys.sortedDescending().forEach {
                    dataList[it]?.run {
                        modelDictInfo.addItem(it, this)//itemList.add(it, this)
                    }
                }
            }
            DataUpdateMode.update -> {
                dataList.forEach { t, u ->
                    modelDictInfo.updateItem(t, u)
                    if (t < modelDictInfo.itemList.count())
                        modelDictInfo.updateItem(t, u)//itemList[t] = u
                }
            }
            DataUpdateMode.delete -> {
                dataList.forEach { t, _ ->
                    modelDictInfo.itemList[t].state = DataUpdateMode.delete
                }
            }
            else -> {
            }
        }

        updateCachedLanguageModelDictItem(modelDictInfo)
    }

    fun deleteLanguageModelEntities(modelId: LanguageModelEntityId) {
        //  remove from cache
        cachedModel.invalidate(modelId.cacheKey())

        //  remove database
        dataManagementService.removeEntity(
                findLanguageModelEntityDb(modelId)
        )
    }

    fun deleteLanguageModelDictionary(modelId: LanguageModelDictId) {
        cachedModel.invalidate(modelId.cacheKey())

        dataManagementService.removeDictionary(
                findLanguageModelDictionaryDb(modelId)
        )
    }

    private fun updateCachedLanguageModelEntityItem(entity: CachedLanguageModelItemEntityInfo) {
        val languageModelEntity = entity.entity

        languageModelEntity.name = entity.name
        languageModelEntity.fileDataKey = entity.fileDataKey
        languageModelEntity.headers = "@fields:${entity.fields.drop(1).joinToString("|") { it.replace("|", "\\|") }}"
        dataService.save(languageModelEntity)

        if (languageModelEntity.fileDataKey.isEmpty()) {
            val removedItems = entity.itemList.filter { it.state == DataUpdateMode.delete }
            val updatedItems = entity.itemList.filter { it.state == DataUpdateMode.update }
            val addedItems = entity.itemList.filter { it.state == DataUpdateMode.insert }

            dataService.deleteEntityItemByIds(removedItems.map { it.id })

            updatedItems.forEach { itemInfo ->
                dataService.findEntityItemById(itemInfo.id)?.let {
                    it.dataItem = entity.getEntityItemDbData(itemInfo)
                    dataService.save(it)
                }
            }

            addedItems.forEach {
                val dataString = entity.getEntityItemDbData(it)
                val dbEntity = LanguageModelEntityItem.createEntityItem(dataString, languageModelEntity)
                dataService.save(dbEntity)
            }
        } else {
            File(File(dataPath), entity.fileDataKey).bufferedWriter().use { bw ->
                bw.write(languageModelEntity.headers + "\n")
                entity.itemList.forEach {
                    val data = it.data
                    val writeLine = entity.getEntityItemDbData(data)
                    bw.write(writeLine + "\n")
                }
            }
        }
    }

    private fun updateCachedLanguageModelDictItem(dictInfo: CachedLanguageModelDictInfo) {
        val languageModelDict = dictInfo.dict

        languageModelDict.name = dictInfo.name
        languageModelDict.fileDataKey = dictInfo.fileDataKey
        languageModelDict.headers = CachedLanguageModelDictInfo.getHeader(dictInfo)
        dataService.save(languageModelDict)

        if (languageModelDict.fileDataKey.isEmpty()) {
            val removedItems = dictInfo.itemList.filter { it.state == DataUpdateMode.delete }
            val updatedItems = dictInfo.itemList.filter { it.state == DataUpdateMode.update }
            val addedItems = dictInfo.itemList.filter { it.state == DataUpdateMode.insert }

            dataService.deleteEntityItemByIds(removedItems.map { it.id })

            updatedItems.forEach { itemInfo ->
                dataService.findEntityItemById(itemInfo.id)?.let {
                    it.dataItem = dictInfo.getDictItemDbData(itemInfo)
                    dataService.save(it)
                }
            }

            addedItems.forEach {
                val dataString = dictInfo.getDictItemDbData(it)
                val dbEntity = LanguageModelDictionaryItem.createLanguageModelDictionaryItem(dataString, languageModelDict)
                dataService.save(dbEntity)
            }
        } else {
            File(File(dataPath), dictInfo.fileDataKey).bufferedWriter().use { bw ->
                bw.write(languageModelDict.headers + "\n")
                dictInfo.itemList.forEach {
                    val data = it.data
                    val writeLine = dictInfo.getDictItemDbData(data)
                    bw.write(writeLine + "\n")
                }
            }
        }
    }

    private fun findAllLanguageModelListByWorkspace(workspaceId: Long, revId: Long? = null): List<LanguageModel> {
        val revision = dataService.findRevision(workspaceId, revId) ?: let {
            logger.warn("Can't find workspace and revision ($workspaceId/$revId)")
            throw InvalidRevisionException(workspaceId, revId, "Can't find revision ($revId) in workspace($workspaceId)")
        }

        return dataService.findLanguageModelListByRevisionId(revision.id)
                ?: throw InvalidLanguageModelException("no language model defined in ${revision.id}")
    }

    fun findAllContextByRevision(workspaceId: Long, revId: Long? = null): List<CachedLanguageModelContext> {
        val languageModelList = findAllLanguageModelListByWorkspace(workspaceId, revId)
        return languageModelList.mapNotNull { lm ->
            val stories = dataService.findAllStoriesByLanguageModel(lm.id)

            stories?.map {
                CachedLanguageModelContext(lm, it)
            }
        }.flatten()
    }

    private fun getCachedLanguageModelStoryByContextId(contextId: Long): CachedLanguageModelContext {
        val story = dataService.findStoryById(contextId)
                ?: throw InvalidLanguageModelContextException("can't find story by id - $contextId")
        val model = story.languageModel
                ?: throw InvalidLanguageModelContextException("can't find story parents language model by id - $contextId")

        return CachedLanguageModelContext(model, story)
    }

    fun setContextOrder(workspaceId: Long, contextOrderList: List<ContextOrderInfo>): List<CachedLanguageModelContext> {
        val contextList = findAllContextByRevision(workspaceId)
        val basePriority = contextList.minBy { it.priority }?.priority ?: 1000
        val contextPriorityStep = 1000

        contextOrderList.forEachIndexed { index, orderInfo ->
            contextList.find { it.appName == orderInfo.appName && it.id == orderInfo.id }?.run {
                this.priority = basePriority + index * contextPriorityStep
                this.updateData()
                dataService.save(this.story)
            }
        }

        return contextList
    }

    fun createContext(workspaceId: Long, params: CreateContext): CachedLanguageModelContext {
        val model = findAllLanguageModelListByWorkspace(workspaceId).filter {
            it.appName == params.appName && it.variant == params.variant && it.language == params.language
        }.first()

        val story = LanguageModelStory()
        story.name = (params.contextName) + ".yaml"
        story.header = LanguageModelStoryHeader(params.contextName)
        story.languageModel = model

        return CachedLanguageModelContext(
                model,
                dataService.save(story))
    }

    private fun isValidUser(story: LanguageModelStory, user: User): Boolean {
        return (story.lockBy == null || story.lockBy?.id == user.id || userService.isAdminUser(user))
    }

    fun modifyContext(workspaceId: Long, contextId: Long, params: ModifyContext, user: User): CachedLanguageModelContext {
        val story = dataService.findStoryById(contextId)
                ?: throw InvalidLanguageModelContextException("can't find story by id - $contextId")
        val model = story.languageModel
                ?: throw InvalidLanguageModelContextException("can't find story parents language model by id - $contextId")

        if (!isValidUser(story, user)) throw InvalidUserException("context is lock by another (${user.email})")

        params.contextName?.let {
            story.name = "$it.yaml"
            story.header = LanguageModelStoryHeader(it)
        }

        if (params.variant != null && params.language != null && (params.contextType != null || params.appName != null)) {
            //  Change LanguageModel Base
            val newModelList = findAllLanguageModelListByWorkspace(workspaceId).filter {
                it.variant == params.variant && it.language == params.language && (it.appName == params.contextType || it.appName == params.appName)
            }

            if (newModelList.isEmpty())
                throw InvalidLanguageModelContextException("Can't find language model with [${params.variant}/${params.language}/${params.appName}/${params.contextType}")

            val newModel = newModelList.first()
            story.languageModel = newModel
        }

        return CachedLanguageModelContext(
                model,
                dataService.save(story)
        )
    }

    fun removeContext(workspaceId: Long, contextId: Long, user: User): Boolean {
        val story = dataService.findStoryById(contextId)
                ?: throw InvalidLanguageModelContextException("can't find story by id - $contextId")

        if (!isValidUser(story, user)) throw InvalidUserException("context is lock by another (${user.email})")

        dataManagementService.removeStory(story)

        return true
    }

    fun lockContextByUser(workspaceId: Long, contextId: Long, lock: Boolean, user: User): CachedLanguageModelContext {
        val cachedContext = getCachedLanguageModelStoryByContextId(contextId)

        if (lock) {
            cachedContext.lockBy = user
            cachedContext.updateData()

            dataService.save(cachedContext.story)
        } else {
            cachedContext.lockBy?.run {
                if (isValidUser(cachedContext.story, user)) {
                    cachedContext.lockBy = null
                    cachedContext.updateData()
                    dataService.save(cachedContext.story)
                }
            }
        }

        return cachedContext
    }

    fun moveContext(workspaceId: Long, cid1: Long, cid2: Long, before: Boolean, user: User): List<CachedLanguageModelContext> {
        val contextList = findAllContextByRevision(workspaceId)

        val baseContext = contextList.find { it.id == cid1 }?.let {
            ContextOrderInfo(it.appName, it.id)
        } ?: throw InvalidLanguageModelContextException("can't find base id ($cid1)")
        val otherContext = contextList.find { it.id == cid2 }?.let {
            ContextOrderInfo(it.appName, it.id)
        } ?: throw InvalidLanguageModelContextException("can't find other id ($cid2)")

        val orderList = contextList.sortedBy { it.priority }.map { ContextOrderInfo(it.appName, it.id) }.toMutableList()
        orderList.removeIf { it.id == otherContext.id }
        val baseIndex = orderList.indexOf(baseContext) + if (before) 0 else 1
        orderList.add(baseIndex, otherContext)

        val reordered = setContextOrder(workspaceId, orderList)

        return reordered.filter { it.id == baseContext.id || it.id == otherContext.id }
    }

    fun getIntentList(workspaceId: Long, cid: Long): List<CachedLanguageModelIntent> {
        val cachedContext = getCachedLanguageModelStoryByContextId(cid)

        return dataService.findAllStoryItemsByStory(cachedContext.id)?.map {
            CachedLanguageModelIntent(cachedContext.story, it)
        } ?: listOf()
    }

    fun findAllIntentListByContext(cid: Long): List<CachedLanguageModelIntent> {
        val cachedContext = getCachedLanguageModelStoryByContextId(cid)

        return dataService.findAllStoryItemsByStory(cachedContext.id)?.map {
            CachedLanguageModelIntent(cachedContext.story, it)
        } ?: listOf()
    }

    fun setIntentOrder(workspaceId: Long, cid: Long, intentIdList: List<Long>, user: User): List<CachedLanguageModelIntent> {
        val patternPriorityStep = 5
        val intentList = findAllIntentListByContext(cid)

        if (intentList.isEmpty())
            return listOf()

        if (!isValidUser(intentList.first().story, user)) throw InvalidUserException("context is lock by another (${user.email})")

        return intentIdList.mapIndexedNotNull { idx, id ->
            intentList.find { id == it.id }?.let {
                it.priority = idx * patternPriorityStep
                it.updateData()
                dataService.save(it.storyItem)
                it
            }
        }
    }

    fun moveIntent(workspaceId: Long, cid: Long, pid: Long, ppid: Long, before: Boolean, user: User): List<CachedLanguageModelIntent> {
        val intentList = findAllIntentListByContext(cid)

        if (intentList.isEmpty())
            return listOf()

        if (!isValidUser(intentList.first().story, user)) throw InvalidUserException("context is lock by another (${user.email})")

        val baseIntent = intentList.find { it.id == pid }
                ?: throw InvalidLanguageModelContextException("can't find intent id ($pid)")
        val otherIntent = intentList.find { it.id == ppid }
                ?: throw InvalidLanguageModelContextException("can't find p-intent id ($ppid)")

        otherIntent.priority = baseIntent.priority + if (before) 0 else 1
        otherIntent.updateData()
        dataService.save(otherIntent.storyItem)

        return listOf(baseIntent, otherIntent)
    }

    fun removeIntent(workspaceId: Long, cid: Long, pid: Long, user: User): Boolean {
        val intentList = findAllIntentListByContext(cid)
        val intent = intentList.find { it.id == pid }
                ?: throw InvalidLanguageModelContextException("can't find intent id ($pid)")

        if (!isValidUser(intent.story, user)) throw InvalidUserException("context is lock by another (${user.email})")

        dataService.delete(intent.storyItem)

        return true
    }

    fun createIntent(workspaceId: Long, cid: Long, params: CreateIntentInfo, user: User): CachedLanguageModelIntent {
        val context = getCachedLanguageModelStoryByContextId(cid)
        val intent = LanguageModelStoryItem()
        intent.languageModelStory = context.story
        intent.intent.let {
            it.name = params.name
            it.patterns = listOf(params.pattern)
            it.hints = params.hints ?: listOf()
            it.matchClass = params.matchClass
            it.priority = 0

            val actionParamData = ActionParamService.getActionParamFromParamList(context.variant, params.actionName, params.actionParams)
            val actionParamIntents = ActionParamIntents(params.actionName, actionParamData)

            it.action = ActionParamService.getActionFromActionParam(actionParamIntents)
        }

        dataService.save(intent)

        return CachedLanguageModelIntent(context.story, intent)
    }

    fun modifyIntent(workspaceId: Long, cid: Long, pid: Long, params: UpdateIntentInfo, user: User): CachedLanguageModelIntent {
        val context = getCachedLanguageModelStoryByContextId(cid)
        val intentList = findAllIntentListByContext(cid)

        if (intentList.isEmpty()) throw InvalidLanguageModelContextException("intent list in context($cid) is empty")
        if (!isValidUser(intentList.first().story, user)) throw InvalidUserException("context is lock by another (${user.email})")

        val intent = intentList.find { it.id == pid }
                ?: throw InvalidLanguageModelContextException("can't find intent $pid")

        params.name?.run { intent.name = this }
        params.hints?.run { intent.hints = this }
        params.pattern?.run { intent.patterns = listOf(this) }
        params.matchClass?.run { intent.matchClass = this }

        if (params.actionName != null) {
            val actionParamData = ActionParamService.getActionParamFromParamList(context.variant, params.actionName, params.actionParams)
            val actionParamIntents = ActionParamIntents(params.actionName, actionParamData)

            intent.action = ActionParamService.getActionFromActionParam(actionParamIntents)
            intent.actionParam = actionParamIntents
        }

        intent.updateData()
        dataService.save(intent.storyItem)

        return intent
    }

    //  revId가 존재하지 않으면 마지막 릴리즈 버젼을 다시 릴리즈 (entity updated)
    //  revId가 존재하면, 해당 리비젼은 deploy에 복사후 릴리즈
    fun deployRevision(revId: Long?, messages: String?): File? {
        logger.debug("deployRevision with $revId [$messages]")

        val deployWorkspace = dataService.findWorkspaceByEditable(false) ?: let {
            val user = userService.currentUser()
            dataService.save(LanguageModelWorkspace(name = "release", editable = false, owner = user))
        }

        if (revId != null) {
            logger.debug("release specific revision $revId")
            val currentUser = userService.currentUser() ?: throw InvalidUserException("can't find current user")
            val revision = dataService.findRevision(revId) ?: let {
                logger.warn("Can't find revision ($revId)")
                throw InvalidRevisionException(0, revId, "Can't find revision ($revId)")
            }

            val deployRevision = dataService.save(
                    LanguageModelRevision(working = false, languageModelWorkspace = deployWorkspace,
                            comments = messages ?: "deployed by ${currentUser.userId}", user = currentUser)
            )

            dataManagementService.copyRevision(revision, deployRevision, null, null)
            revision.deployRevisionId = deployRevision.id
            dataService.save(revision)
        }

        //  Deploy워크스페이스의 마지막 리비젼 가져오기
        val deployRevision = dataService.findAllRevisionsByWorkspace(deployWorkspace.id)?.sortedByDescending {
            it.commitDate
        }?.first() ?: throw InvalidRevisionException(0, 0, "Can't find revision in deploy workspace")

        return modelExchanger.exportToFiles(deployWorkspace.id, deployRevision.id)
    }
}
