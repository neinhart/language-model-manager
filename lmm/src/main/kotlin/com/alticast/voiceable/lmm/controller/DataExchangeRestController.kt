/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.controller
      Date  : 19. 3. 7
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.controller

import com.alticast.voiceable.lmm.jwt.ApiKeySecured
import com.alticast.voiceable.lmm.service.exchange.LanguageModelExchanger
import io.swagger.annotations.ApiOperation
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.InputStreamResource
import org.springframework.core.io.InputStreamSource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import java.io.FileInputStream
import javax.servlet.http.HttpServletResponse

@RestController
class DataExchangeRestController {
    private val logger = LoggerFactory.getLogger(DataExchangeRestController::class.java)

    @Autowired
    lateinit var languageModelExchanger: LanguageModelExchanger

    @ApiKeySecured
    @ApiOperation(value = "zip형태의 언어모델을 Import한다.", notes = "zip은 name/variant/language...형태로 들어가야 한다.")
    @RequestMapping(value = ["/v2/operation/import", "/operation/import"], method = arrayOf(RequestMethod.POST))
    fun importLanguageModels(@RequestParam("file") file: MultipartFile): Any {
        return languageModelExchanger.importFromZipStream(file.inputStream)
    }

    @ApiKeySecured
    @ApiOperation(value = "특정 Workspace / Revision을 비탕으로 zip형태로 언어모델 데이타를 생성한다")
    @RequestMapping(value = ["/v2/operation/export", "/operation/export"], method = arrayOf(RequestMethod.GET))
    fun exportLanguageModels(@RequestParam("workspaceId") workspaceId: Long,
                             @RequestParam("revisionId", required = false) revisionId: Long?,
                             response: HttpServletResponse): ResponseEntity<InputStreamSource> {
        val zippedFile = languageModelExchanger.exportToFiles(workspaceId, revisionId)
        val resource = InputStreamResource(FileInputStream(zippedFile))

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=lmm.$workspaceId.${revisionId
                        ?: "default"}.zip")
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .contentLength(zippedFile.length())
                .body(resource)
    }
}