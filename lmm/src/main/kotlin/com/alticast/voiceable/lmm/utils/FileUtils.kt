/*
    Copyright (C) 2018 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.service
      Date  : 18. 11. 13
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.utils

import org.slf4j.LoggerFactory
import org.springframework.util.DigestUtils
import java.io.File
import java.io.IOException
import java.nio.file.FileVisitResult
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.SimpleFileVisitor
import java.nio.file.StandardCopyOption
import java.nio.file.attribute.BasicFileAttributes

object FileUtils {
    private val logger = LoggerFactory.getLogger(FileUtils.javaClass)

    fun copyDataFrom(variant: String, language: String, sourcePath: String, destPath: String): List<String> {
        val source = File(sourcePath).toPath()
        val dest = File(File(destPath), "/$variant/$language").toPath()

        return copyTreeIfChanged(source, dest)
    }

    fun copyTreeIfChanged(source: Path, dest: Path): List<String> {
        val updatedDataList = mutableListOf<String>()

        Files.walkFileTree(source,
                object : SimpleFileVisitor<Path>() {
                    @Throws(IOException::class)
                    override fun preVisitDirectory(
                            dir: Path,
                            attr: BasicFileAttributes
                    ): FileVisitResult {
                        val destPath = dest.resolve(source.relativize(dir))
                        Files.createDirectories(destPath)
                        return FileVisitResult.CONTINUE
                    }

                    @Throws(IOException::class)
                    override fun visitFile(
                            file: Path,
                            attr: BasicFileAttributes
                    ): FileVisitResult {

                        val destPath = dest.resolve(source.relativize(file))
                        val inputStream = Files.newInputStream(file)
                        val sourceMd5 = DigestUtils.md5DigestAsHex(inputStream)
                        inputStream.close()
                        val destMd5 =
                                if (Files.exists(destPath)) {
                                    val dInput = Files.newInputStream(destPath)
                                    val res = DigestUtils.md5DigestAsHex(dInput)
                                    dInput.close()

                                    res
                                } else "NotFound"

                        if (sourceMd5 != destMd5) {
                            logger.info("data is updated : $file")
                            updatedDataList.add(file.toString())
                            Files.copy(
                                    file, destPath,
                                    StandardCopyOption.COPY_ATTRIBUTES,
                                    StandardCopyOption.REPLACE_EXISTING
                            )
                        }

                        return FileVisitResult.CONTINUE
                    }
                })


        return updatedDataList
    }
}
