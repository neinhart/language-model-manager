/*
    Copyright (C) 2018 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.languagemodelmanager.jwt
      Date  : 18. 9. 18
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.jwt

import java.lang.annotation.Inherited


@MustBeDocumented
@Inherited
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@Retention(AnnotationRetention.RUNTIME)
annotation class ApiKeySecured(val mandatory: Boolean = false)
