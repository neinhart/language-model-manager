/*
    Copyright (C) 2018 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.langmo
      Date  : 18. 9. 18
     Author : kimjh 
*/
package com.alticast.voiceable.lmm

import com.alticast.voiceable.lmm.jwt.ExposeResponseInterceptor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter


@Configuration
class WebMvcConfig : WebMvcConfigurerAdapter() {
    @Autowired
    private var interceptor: ExposeResponseInterceptor? = null

    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(interceptor!!).addPathPatterns("/**")
    }
}