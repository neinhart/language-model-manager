/*
    Copyright (C) 2018 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.langmo.jwt
      Date  : 18. 9. 18
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.jwt

import org.springframework.stereotype.Component
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse



@Component
class ExposeResponseInterceptor: HandlerInterceptorAdapter() {
    @Throws(Exception::class)
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        request.setAttribute(KEY, response)
        return true
    }

    companion object {
        val KEY = "HttpResponseKey"
    }
}