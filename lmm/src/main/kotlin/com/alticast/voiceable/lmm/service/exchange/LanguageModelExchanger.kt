/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.service.exchange
      Date  : 19. 3. 7
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.service.exchange

import com.alticast.voiceable.lmm.exceptions.InvalidRevisionException
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModel
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelDictionary
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelDictionaryItem
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelEntity
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelEntityItem
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelPrivate
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelRevision
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelStory
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelWorkspace
import com.alticast.voiceable.lmm.service.DataService
import com.alticast.voiceable.lmm.service.MatcherService
import com.alticast.voiceable.lmm.service.UserService
import com.alticast.voiceable.lmm.service.cached.LanguageModelManagementService
import com.alticast.voiceable.lmm.utils.ZipDirectory
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.io.File
import java.io.InputStream
import java.util.UUID
import javax.annotation.PostConstruct

data class UpdateEntityInfo(val fullPath: String, val variant: String, val language: String, val appName: String, val dataType: String, val dataName: String)

@Service
class LanguageModelExchanger {
    private val logger = LoggerFactory.getLogger(LanguageModelExchanger::class.java)

    @Autowired
    lateinit var dataService: DataService

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var modelManagementService: LanguageModelManagementService

    @Autowired
    lateinit var matcherService: MatcherService

    @Value("\${lmm.entity.path}")
    lateinit var entityDataDir: String

    @Value("\${lmm.custom-tag}")
    lateinit var customTagList: Array<String>

    @PostConstruct
    fun init() {
        val entityDir = File(entityDataDir)
        if (!entityDir.isDirectory)
            entityDir.mkdirs()
    }

    private fun createTempDataDir() = File("/tmp/.voiceable/lmm/${UUID.randomUUID()}").apply { this.mkdirs() }
    private fun removeTempDataDir(dir: File) {
        if (dir.isDirectory)
            dir.deleteRecursively()
    }

    private fun isCustomTags(dataName: String) = customTagList.any { dataName.contains(it) }

    //  Deploy용도의 Workspace가 없으면 Workspace 하나를 생성한다.
    fun createReleaseWorkspace() {
        dataService.findWorkspaceByEditable(false) ?: let {
            val user = userService.currentUser()
            dataService.save(LanguageModelWorkspace(name = "release", editable = false, owner = user))

            logger.debug("Deploy workspace is created")
        }
    }

    fun getFileDataKey(variant: String, language: String, appName: String, modelType: String, dataName: String): String {
        return if (!isCustomTags(dataName)) "$variant.$language.$appName.$modelType.$dataName.dat" else ""
    }

    //  Auto-gen (from EGen)이므로, /lmm/entity내의 shared file을 업데이트 한다.
    fun updateEntities(updateInfoList: List<UpdateEntityInfo>) {
        updateInfoList.forEach { info ->
            val fileDataKey = getFileDataKey(info.variant, info.language, info.appName, info.dataType, info.dataName)
            copyFile(File(info.fullPath), File(File(entityDataDir), fileDataKey))

        }
        logger.debug("update entities are done - set flag for cache clear")
        matcherService.invalidateCache()
        modelManagementService.invalidateCache()
    }

    fun importFromZipStream(stream: InputStream): Any {
        val extractedDir = createTempDataDir()

        logger.info("import from zip : extract data to $extractedDir")
        ZipDirectory.unpackFromZip(stream, extractedDir)

        val newWorkspace = dataService.save(
                LanguageModelWorkspace(name = "imported", editable = true, owner = userService.currentUser())
        )

        val languageModelRevision = dataService.save(
                LanguageModelRevision(comments = "language model is imported.", languageModelWorkspace = newWorkspace, working = true)
        )
        val languageModelList = mutableListOf<LanguageModel>()
        for (file in extractedDir.walkTopDown()) {
            if (file.isFile) {
                val dataIds = file.relativeTo(extractedDir).path.split("/").drop(1)
                if (dataIds.count() != 5) {
                    logger.warn("import data structure is not valid - id = $dataIds")
                    continue
                }
                val (variant, language, appName, modelType, dataName) = dataIds

                val languageModel = languageModelList.find {
                    it.variant == variant && it.language == language && it.appName == appName
                } ?: let {
                    val model = LanguageModel(variant = variant, language = language, appName = appName, languageModelRevision = languageModelRevision)
                    languageModelList.add(model)
                    dataService.save(model)

                    model
                }

                val fileDataKey = getFileDataKey(variant, language, appName, modelType, dataName)

                logger.debug("import - $file")
                when (file.extension) {
                    "entity" -> {
                        val entityItemList = mutableListOf<String>()
                        val modelEntity = LanguageModelEntity.createEntityModel(languageModel, fileDataKey, file,
                                onEntity = { model ->
                                    dataService.save(model)
                                },
                                onEntityItem = { model, data ->
                                    if (fileDataKey.isEmpty()) {
                                        dataService.save(
                                                LanguageModelEntityItem(dataItem = data, languageModelEntity = model)
                                        )
                                    } else {
                                        entityItemList.add(data)
                                    }
                                })

                        //  entityItemList가 채워져 있으면, fileDataKey의 존재를 의미하고 이는 DB가 아닌 File로 저장함을 의미한다.
                        if (entityItemList.isNotEmpty()) {
                            val entityFile = File(File(entityDataDir), fileDataKey)
                            entityFile.bufferedWriter().use { bw ->
                                bw.write(modelEntity.headers +"\n")

                                entityItemList.forEach {
                                    bw.write(it + "\n")
                                }
                            }
                        }
                    }
                    "dict" -> {
                        val dictionaryItemList = mutableListOf<String>()
                        val modelDict = LanguageModelDictionary.createDictionaryModel(languageModel, fileDataKey, file,
                                onDictionary = { dictionary ->
                                    dataService.save(dictionary)
                                },
                                onDictionaryItem = { model, data ->
                                    if (fileDataKey.isEmpty()) {
                                        dataService.save(
                                                LanguageModelDictionaryItem(dataItem = data, languageModelDictionary = model)
                                        )
                                    } else {
                                        dictionaryItemList.add(data)
                                    }
                                })

                        if (dictionaryItemList.isNotEmpty()) {
                            val dictionaryFile = File(File(entityDataDir), fileDataKey)
                            dictionaryFile.bufferedWriter().use { bw ->
                                bw.write(modelDict.headers +"\n")
                                dictionaryItemList.forEach {
                                    bw.write(it + "\n")
                                }
                            }
                        }
                    }
                    "yaml" -> {
                        LanguageModelStory.createStoryModel(languageModel, fileDataKey, file,
                                onStory = { story ->
                                    dataService.save(story)
                                },
                                onStoryItem = { story, storyItem ->
                                    dataService.save(storyItem)
                                })
                    }
                    else -> {
                        dataService.save(
                                LanguageModelPrivate.createPrivateModel(languageModel, fileDataKey, file,
                                        onPrivateData = { model, data ->
                                            val privateFile = File(File(entityDataDir), fileDataKey)
                                            privateFile.bufferedWriter().use {
                                                it.write(data)
                                            }
                                        })
                        )
                    }
                }
            }
        }

        logger.debug("import completed")

        removeTempDataDir(extractedDir)

        return mapOf(
                "status" to "ok",
                "workspaceId" to newWorkspace.id,
                "revisionId" to languageModelRevision.id
        )
    }

    private fun findAllModelListByRevisionId(revisionId: Long): List<LanguageModel> {
        return dataService.findLanguageModelListByRevisionId(revisionId)
                ?: throw InvalidRevisionException(-1, revisionId, "empty language models in Unknown, $revisionId")
    }

    private fun copyFile(src: File, dst: File, header: String = "") {
        if (!dst.parentFile.isDirectory)
            dst.parentFile.mkdirs()

        src.bufferedReader().use { sr ->
            val text = sr.readText()

            dst.bufferedWriter().use {
                if (header.isNotEmpty())
                    it.write(header + "\n")
                it.write(text)
            }
        }
    }

    fun exportToFiles(workspaceId: Long, revisionId: Long?): File {
        logger.debug("export workspace $workspaceId, revision $revisionId to zip file")

        val revision = dataService.findRevision(workspaceId, revisionId)
                ?: throw InvalidRevisionException(workspaceId, revisionId, "Can't find $workspaceId/$revisionId in revision List")

        val baseDir = createTempDataDir()
        findAllModelListByRevisionId(revision.id).forEach { model ->
            val dataDir = File(baseDir, "languagemodel/${model.variant}/${model.language}/${model.appName}")
            logger.debug("exporting - $model")

            //  find and write entity model
            dataService.findAllEntitiesByLanguageModel(model.id)?.forEach { entityInfo ->
                if (entityInfo.fileDataKey.isEmpty()) {
                    val entityItemList = dataService.findAllEntityItemsByEntity(entityInfo.id)

                    LanguageModelEntity.writeEntityModel(dataDir, entityInfo, entityItemList ?: listOf())
                } else {
                    copyFile(File(File(entityDataDir), entityInfo.fileDataKey), File(dataDir, "/${LanguageModelEntity.baseDir}/${entityInfo.name}"), header = "")//entityInfo.headers)
                }
            }

            dataService.findAllDictionariesByLanguageModel(model.id)?.forEach { dictInfo ->
                if (dictInfo.fileDataKey.isEmpty()) {
                    val dictItemList = dataService.findAllDictionaryItemsByDictionary(dictInfo.id)

                    LanguageModelDictionary.writeDictionaryModel(dataDir, dictInfo, dictItemList ?: listOf())
                } else {
                    copyFile(File(File(entityDataDir), dictInfo.fileDataKey), File(dataDir, "/${LanguageModelDictionary.baseDir}/${dictInfo.name}"), header = "")//dictInfo.headers)
                }
            }
            dataService.findAllPrivatesByLanguageModel(model.id)?.forEach { privateInfo ->
                if (privateInfo.fileDataKey.isEmpty()) {
                    LanguageModelPrivate.writePrivateModel(dataDir, privateInfo)
                } else {
                    copyFile(File(File(entityDataDir), privateInfo.fileDataKey), File(dataDir, "/${LanguageModelPrivate.baseDir}/${privateInfo.name}"))
                }
            }

            dataService.findAllStoriesByLanguageModel(model.id)?.forEachIndexed { idx, storyInfo ->
                val storyItems = dataService.findAllStoryItemsByStory(storyInfo.id)
                LanguageModelStory.writeStoryModel(dataDir, storyInfo, storyItems, idx)
            }
        }

        val zippedFile = File(baseDir.parent, "lm.zip")
        ZipDirectory.packToZip(baseDir.path, zippedFile.path)

        baseDir.deleteRecursively()
        logger.debug("export - finished ($zippedFile)")
        return zippedFile
    }
}