/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.repository.datamodel
      Date  : 19. 3. 12
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.repository.datamodel

import com.alticast.voiceable.lmm.utils.Story
import com.alticast.voiceable.lmm.utils.StoryCondition
import com.alticast.voiceable.lmm.utils.StoryContext
import com.alticast.voiceable.lmm.utils.StoryParser
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.io.File
import javax.persistence.AttributeConverter
import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Lob
import javax.persistence.ManyToOne
import javax.persistence.OneToOne

data class LanguageModelStoryHeader(
        val name: String = "",
        val priority: Int = 0,
        val conditions: List<StoryCondition> = mutableListOf(),
        val tags: List<String> = mutableListOf(),
        val timeout: Int = 0)

@Convert
class StoryHeaderConverter : AttributeConverter<LanguageModelStoryHeader?, String?> {
    override fun convertToDatabaseColumn(attribute: LanguageModelStoryHeader?): String? {
        attribute ?: return null
        val retString = ObjectMapper().writeValueAsString(attribute)

        return retString
    }

    override fun convertToEntityAttribute(dbData: String?): LanguageModelStoryHeader? {
        dbData ?: return null

        val ret = ObjectMapper().readValue<LanguageModelStoryHeader>(dbData)
        return ret
    }
}

@Entity
data class LanguageModelStory(
        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0,

        var name: String = "",

        @Lob
        @Convert(converter = StoryHeaderConverter::class)
        var header: LanguageModelStoryHeader? = null,

        @OneToOne
        var lockBy: User? = null,

        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @OnDelete(action = OnDeleteAction.CASCADE)
        @JsonIgnore
        var languageModel: LanguageModel? = null
) {
    companion object {
        val baseDir = "stories"

        fun createStoryModel(parentModel: LanguageModel, dataKey: String, dataFile: File,
                             onStory: (story: LanguageModelStory) -> Unit,
                             onStoryItem: (story: LanguageModelStory, data: LanguageModelStoryItem) -> Unit): List<LanguageModelStory> {
            val contextList = StoryParser.parse(dataFile)

            return contextList.map { context ->
                val header = LanguageModelStoryHeader(
                        name = context.getContextName(),
                        priority = context.getContextPriority(),
                        conditions = context.getContextConditions(),
                        tags = context.getContextTags(),
                        timeout = context.getContextTimeout()
                )
                val modelStory = LanguageModelStory(header = header, languageModel = parentModel, name = dataFile.name).apply {
                    onStory(this)
                }

                val intentList = context.getContextIntents()
                intentList.forEach { intent ->
                    val storyIntent = LanguageModelStoryItemIntent.convertStoryIntentToLanguageModelIntent(intent)

                    LanguageModelStoryItem(intent = storyIntent, languageModelStory = modelStory).run {
                        onStoryItem(modelStory, this)
                    }
                }

                modelStory
            }
        }

        private fun generateStoryFileName(parent: File, name: String): File {
            val baseName = name.split(".").dropLast(1).joinToString(".")
            val extension = name.split(".").takeLast(1)

            if (!File(parent, name).isFile)
                return File(parent, name)

            var index = 0
            while (true) {
                val f = File(parent, "$baseName.$index.$extension")
                if (!f.isFile)
                    return f
                index++
            }
        }

        fun writeStoryModel(dataDir: File, story: LanguageModelStory, storyItems: List<LanguageModelStoryItem>?, idx: Int) {
            val conditions = story.header?.conditions
            val name = story.header?.name
            val priority = story.header?.priority
            val tags = story.header?.tags
            val timeout = story.header?.timeout
            val intents = storyItems?.mapIndexed { idx, it ->
                LanguageModelStoryItemIntent.convertLanguageModelIntentToStoryIntent(idx, it.intent)
            }?.toMutableList()

            val context = StoryContext(
                    condition = null, conditions = conditions,
                    intent = null, intents = intents,
                    alias = null, aliases = null,
                    componentPriority = null,
                    name = name,
                    tag = null, tags = tags,
                    timeout = timeout, priority = priority)

            val parentDir = File(dataDir, baseDir)
            if (!parentDir.isDirectory)
                parentDir.mkdirs()

            //val headerStory = "$idx.${story.header?.name?.let { it.filter { it.isLetterOrDigit() } + ".yaml" } ?: story.name}"
            val headerStory = "$idx.${story.languageModel?.appName ?: "context"}.yaml"
            val dstFile = generateStoryFileName(parentDir, headerStory)
            StoryParser.buildYaml(dstFile, Story(context = context, contexts = null, intent = null))
        }

        fun createLanguageModelStory(name: String, header: LanguageModelStoryHeader?, model: LanguageModel): LanguageModelStory {
            return LanguageModelStory(name = name, header = header, languageModel = model)
        }
    }
}
