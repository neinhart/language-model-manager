/*
    Copyright (C) 2018 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm
      Date  : 18. 10. 30
     Author : kimjh 
*/
package com.alticast.voiceable.lmm

import com.alticast.voiceable.lmm.service.IngestLanguageModel
import com.alticast.voiceable.lmm.service.cached.LanguageModelManagementService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component


@Component
class LmmScheduler {
    private val logger = LoggerFactory.getLogger(LmmScheduler::class.java)

    //@Autowired
    //private val lmmConverter: ImportExportService? = null
    //
    //@Autowired
    //private val workspaceService: WorkspaceService? = null
    //
    //@Value("\${lmm.deploy.path}")
    //private val deployPath: String = "./deploy"
    //

    @Autowired
    lateinit var modelManagementService: LanguageModelManagementService

    @Autowired
    lateinit var ingestLanguageModel: IngestLanguageModel

    @Value("\${lmm.master}")
    private val lmmMaster: Boolean = true

    @Scheduled(cron = "\${lmm.deploy.auto}")
    fun deployAutomatic() {
        if (lmmMaster) {
            logger.info("deploy by schedule")
            modelManagementService.deployRevision(null, null)?.run {
                ingestLanguageModel.ingestLanguageModel(this)
            }
        } else {
            logger.info("lmm is not master mode - ignore auto deploy")
        }
    }
}