/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.service.cached
      Date  : 19. 3. 26
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.service.cached

import com.alticast.voiceable.lmm.repository.datamodel.LanguageModel
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelStory
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelStoryHeader
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty

class CachedLanguageModelContext(
        @JsonIgnore val lm: LanguageModel,
        @JsonIgnore val story: LanguageModelStory) : CachedLanguageModelItem {
    val id = story.id
    var appName = lm.appName
    var variant = lm.variant
    var language = lm.language

    @JsonProperty("name")
    var contextName = story.header?.name ?: story.name.dropLast(5)
    var priority = story.header?.priority ?: 0
    var timeout = story.header?.timeout ?: 0
    var lockBy = story.lockBy

    @JsonIgnore
    val conditions = story.header?.conditions?.toMutableList() ?: mutableListOf()
    @JsonIgnore
    val tags = story.header?.tags?.toMutableList() ?: mutableListOf()

    fun updateData() {
        lm.appName = appName
        lm.variant = variant
        lm.language = language

        story.header =  LanguageModelStoryHeader(contextName, priority, conditions, tags, timeout)
        story.lockBy = lockBy
    }
}