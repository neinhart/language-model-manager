/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.repository.datamodel
      Date  : 19. 3. 12
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.repository.datamodel

import com.alticast.voiceable.lmm.old.model.AuditModel
import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.OneToOne

@Entity
data class LanguageModelRevision(
        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0,

        var working: Boolean = false,
        var comments: String = "",
        var commitDate: LocalDateTime = LocalDateTime.now(),
        var deployRevisionId: Long? = null,

        @OneToOne
        val user: User? = null,

        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @OnDelete(action = OnDeleteAction.CASCADE)
        @JsonIgnore
        var languageModelWorkspace: LanguageModelWorkspace? = null
) : AuditModel()
