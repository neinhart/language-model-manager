package com.alticast.voiceable.lmm.service.cached

import com.alticast.text.utils.StringUtils
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelDictionary

class CachedLanguageModelDictInfo(val dict: LanguageModelDictionary): CachedLanguageModelItem {
    val name = dict.name
    var fileDataKey = dict.fileDataKey
    var dictType: String = getDictType(dict.headers)
    var dictName : String = getDictName(dict.headers)
    var dictSubName: String = getDictSubName(dict.headers)
    var fields: List<String> = getFields(dict.headers)

    data class DictionaryItemInfo(val id: Long, var data: List<String>, var state: DataUpdateMode)
    val itemList = mutableListOf<DictionaryItemInfo>()

    fun addItem(id: Long, data: String): CachedLanguageModelDictInfo {
        val splitted = StringUtils.splitBy(data, ':')
        val speech = splitted[0].replace("\\", "")
        val itemData = listOf(speech) + StringUtils.splitBy(splitted.drop(1).joinToString(":"), '|').map { it.replace("\\", "") }

        itemList.add(DictionaryItemInfo(id, itemData, DataUpdateMode.asitwas))

        return this
    }

    fun addItem(index: Int, data: List<String>): CachedLanguageModelDictInfo {
        itemList.add(index, DictionaryItemInfo(-1, data, DataUpdateMode.insert))
        return this
    }

    fun updateItem(index: Int, data: List<String>): CachedLanguageModelDictInfo {
        val info = itemList[index]

        info.data = data
        info.state = DataUpdateMode.update

        return this
    }

    fun getDictItemDbData(itemInfo: DictionaryItemInfo): String {
        return itemInfo.data.first() + ":" + itemInfo.data.drop(1).joinToString("|") { it.replace("|", "\\|") }
    }

    fun getDictItemDbData(data: List<String>): String {
        return data.first() + ":" + data.drop(1).joinToString("|") {
            it.replace("|", "\\|")
        }
    }

    companion object {
        fun getDictType(header: String): String {
            return header.split(":").first()
        }

        fun getDictName(header: String): String {
            return header.split(":").drop(1).joinToString(":").split("|").first()
        }

        fun getDictSubName(header: String): String {
            val subNameList = header.split(":").drop(1).joinToString(":").split("|").drop(1)
            return if (subNameList.isEmpty()) "" else subNameList.first()
        }

        fun getFields(header: String): List<String> {
            return header.split(":").drop(1).joinToString(":").split("|").drop(1)
        }

        fun getHeader(info: CachedLanguageModelDictInfo): String {
            return "${info.dictType}:${info.dictName}" + if (info.fields.isNotEmpty()) "|${info.fields.joinToString("|")}" else ""
        }
    }
}