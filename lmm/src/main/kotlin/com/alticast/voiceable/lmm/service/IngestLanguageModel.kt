/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.service
      Date  : 19. 4. 9
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.service

import org.apache.http.entity.mime.HttpMultipartMode
import org.apache.http.entity.mime.MultipartEntity
import org.apache.http.entity.mime.content.FileBody
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.io.File
import java.net.HttpURLConnection
import java.net.URL

@Service
class IngestLanguageModel {
    private val logger = LoggerFactory.getLogger(IngestLanguageModel::class.java)

    @Value("\${lmm.deploy.ingest}")
    private val ingestUrl: String = ""

    fun ingestLanguageModel(f: File) {
        val url = URL(ingestUrl)
        val connection = url.openConnection() as HttpURLConnection
        connection.doOutput = true
        connection.requestMethod = "POST"

        val fileBody = FileBody(f)
        val multipartEntity = MultipartEntity(HttpMultipartMode.STRICT)
        multipartEntity.addPart("file", fileBody)

        connection.setRequestProperty("Content-Type", multipartEntity.contentType.value)
        val out = connection.outputStream
        try {
            multipartEntity.writeTo(out)
        } finally {
            out.close()
        }
        val status = connection.responseCode
        //logger.debug("ingest language model")
        //val url = URL("http://localhost:18082/v2/operation/import")//URL(ingestUrl)
        //val conn = url.openConnection() as HttpURLConnection
        //conn.requestMethod = "POST"
        //conn.doOutput = true
        //val data = f.readBytes()
        //
        //conn.addRequestProperty("Content-Length", data.size.toString())
        //conn.setRequestProperty("Content-Type", "application/octet")
        //val outputStream = DataOutputStream(conn.outputStream)
        //outputStream.write(data)
        //outputStream.flush()
        //logger.info("Send data to Ingest - ${conn.responseCode}")
    }

    fun ing() {
    }
}