/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.controller
      Date  : 19. 3. 18
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.controller

import com.alticast.text.entity.SystemEntityManager
import com.alticast.voiceable.lmm.jwt.ApiKeySecured
import com.alticast.voiceable.lmm.service.ActionParamService
import com.alticast.voiceable.lmm.service.DataManagementService
import com.alticast.voiceable.lmm.service.MatcherService
import com.alticast.voiceable.lmm.service.cached.DataUpdateMode
import com.alticast.voiceable.lmm.service.cached.LanguageModelDictId
import com.alticast.voiceable.lmm.service.cached.LanguageModelEntityId
import com.alticast.voiceable.lmm.service.cached.LanguageModelManagementService
import io.swagger.annotations.ApiOperation
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import javax.validation.Valid

data class EntityDataFormat(val fields: List<String>, val mode: DataUpdateMode?, val lines: Map<Int, List<String>>)

data class DictDataFormat(val dictType: String, val dictName: String, val sourceField: String?,
                          val mode: DataUpdateMode?, val dataList: Map<Int, List<String>>)

data class DictDataDeleteFormat(val dictType: String, val dictName: String, val sourceField: String?)
data class DictDataFindFormat(val dictType: String, val dictName: String, val findValue: String)

@RestController
class LanguageModelRestController {
    private val logger = LoggerFactory.getLogger(LanguageModelRestController::class.java)

    @Autowired
    lateinit var dataManagementService: DataManagementService

    @Autowired
    lateinit var actionParamService: ActionParamService

    @Autowired
    lateinit var languageModelManagementService: LanguageModelManagementService

    @Autowired
    lateinit var matcherService: MatcherService

    @Value("#{'\${lmm.support-classes}'.split(',')}")
    lateinit var supportedMatchClassList: List<String>

    private fun sendResult(data: Any) = mapOf("helper" to data)

    @ApiOperation("request list of supported variants")
    @GetMapping("/helper/variants")
    fun getRegisteredVariantList(): Any {
        return sendResult(dataManagementService.getProfileList().keys)
    }

    @ApiOperation("request list of supported languages.")
    @GetMapping("/helper/languages")
    fun getRegisteredLanguageList(): Any {
        return sendResult(dataManagementService.getProfileList().flatMap { it.value }.distinct())
    }

    @ApiOperation("request list of supported match classes.")
    @GetMapping("/helper/matchclasses")
    fun getMatchClasses(): Any {
        return sendResult(supportedMatchClassList)
    }

    @GetMapping("/helper/context-types")
    fun getContextTypes(): Any {
        return sendResult("helper" to listOf("app", "global", "screen"))
    }

    @ApiOperation("지원되는 액션의 목록을 조회한다.")
    @GetMapping("/helper/intents")
    fun getIntentsNameList(variant: String?): Any {
        return sendResult(ActionParamService.getActionParams(variant))
    }

    @ApiOperation("intents 정보 meta 파일(resources/intents/cjhv.yaml) 업로드. 기존 메타정보는 변경되지 않음.")
    @RequestMapping(value = ["/helper/uploadMeta"], method = arrayOf(RequestMethod.POST))
    @ResponseBody
    fun uploadMetaInfo(
            @RequestParam("variant") variant: String,
            @RequestParam("file") file: MultipartFile): Any {

        if (file.isEmpty)
            return sendResult("invalid file uploaded")

        return sendResult("meta ${actionParamService.loadActionParam(variant, file.inputStream)} is loaded")
    }

    @ApiKeySecured
    @GetMapping(value = ["/helper/asr/entity", "/entity/operation"])
    fun getEntities(@RequestParam("workspaceId") workspaceId: Long,
                    @RequestParam("revisionId", required = false) revisionId: Long?,
                    @RequestParam("variant") variant: String,
                    @RequestParam("language") language: String,
                    @RequestParam("appName") appName: String,
                    @RequestParam("dataName") dataName: String,
                    @RequestParam("start", required = false) start: Int?,
                    @RequestParam("count", required = false) count: Int?): Any {
        val entityModelId = LanguageModelEntityId(workspaceId, revisionId, variant, language, appName, dataName)
        val entityModel = languageModelManagementService.findLanguageModelEntity(entityModelId)

        val info = mapOf(
                "total" to entityModel.itemList.count(),
                "fields" to entityModel.fields.count(),
                "readonly" to entityModel.fileDataKey.isNotEmpty()
        )

        return sendResult(mapOf(
                "info" to info,
                "fields" to entityModel.fields,
                "lines" to entityModel.itemList.drop(start ?: 0).take(count ?: 100)
        ))
    }

    @ApiKeySecured
    @GetMapping(value = ["/helper/asr/entitygroup", "/entity"])
    fun getEntityGroupList(@RequestParam("workspaceId") workspaceId: Long,
                           @RequestParam("revisionId", required = false) revisionId: Long?,
                           @RequestParam("variants") variant: String,
                           @RequestParam("language") language: String,
                           @RequestParam("appName") appName: String): Any {

        val modelId = LanguageModelEntityId(workspaceId, revisionId, variant, language, appName, "")
        val appEntityGroupList = languageModelManagementService.findLanguageModelEntityGroupList(modelId)

        return sendResult(
                mapOf(
                        "system" to SystemEntityManager.getGroups(),
                        "app" to appEntityGroupList
                )
        )
    }

    @ApiKeySecured
    @PostMapping(value = ["/helper/asr/entity", "/entity/operation"])
    fun updateEntities(@RequestParam("workspaceId") workspaceId: Long,
                       @RequestParam("revisionId", required = false) revisionId: Long?,
                       @RequestParam("variant") variant: String,
                       @RequestParam("language") language: String,
                       @RequestParam("appName") appName: String,
                       @RequestParam("dataName") dataName: String,
                       @Valid @RequestBody dataFormat: EntityDataFormat): Any {
        val modelId = LanguageModelEntityId(workspaceId, revisionId, variant, language, appName, dataName)

        //  fields --> speech를 제외한 나머지 데이타
        languageModelManagementService.updateLanguageModelEntities(modelId, dataFormat.mode
                ?: DataUpdateMode.update, dataFormat.fields, dataFormat.lines)

        return sendResult(mapOf(
                "status" to "entities are saved"
        ))
    }

    @ApiKeySecured
    @DeleteMapping(value = ["/helper/asr/entity", "/entity/operation"])
    fun deleteEntities(@RequestParam("workspaceId") workspaceId: Long,
                       @RequestParam("revisionId", required = false) revisionId: Long?,
                       @RequestParam("variant") variant: String,
                       @RequestParam("language") language: String,
                       @RequestParam("appName") appName: String,
                       @RequestParam("dataName") dataName: String): Any {
        val modelId = LanguageModelEntityId(workspaceId, revisionId, variant, language, appName, dataName)
        languageModelManagementService.deleteLanguageModelEntities(modelId)

        return sendResult(mapOf(
                "status" to "success"
        ))
    }

    @ApiOperation("Retrieve dictionary name list")      //  "type": [name1, name2, ...]
    @ApiKeySecured
    @GetMapping("/dictionary")
    fun getDictionaryList(@RequestParam("workspaceId") workspaceId: Long,
                          @RequestParam("revisionId", required = false) revisionId: Long?,
                          @RequestParam("variant") variant: String,
                          @RequestParam("language") language: String,
                          @RequestParam("appName") appName: String): Any {
        val modelId = LanguageModelDictId(workspaceId, revisionId, variant, language, appName, "", "")

        return mapOf("dictionary" to languageModelManagementService.findLanguageModelDictionaryGroupList(modelId))
    }

    @ApiOperation("get dictionary data with given params")
    @ApiKeySecured
    @GetMapping("/dictionary/operation")
    fun getDictionaryData(@RequestParam("workspaceId") workspaceId: Long,
                          @RequestParam("revisionId", required = false) revisionId: Long?,
                          @RequestParam("variant") variant: String,
                          @RequestParam("language") language: String,
                          @RequestParam("appName") appName: String,
                          @RequestParam("dictType") dictType: String,
                          @RequestParam("dictName") dictName: String,
                          @RequestParam("start", required = false) start: Int?,
                          @RequestParam("count", required = false) count: Int?): Any {
        val modelId = LanguageModelDictId(workspaceId, revisionId, variant, language, appName, dictType, dictName)

        val dictInfo = languageModelManagementService.findLanguageModelDictionary(modelId)

        return mapOf(
                "dictionary" to mapOf(
                        "fields" to listOf("source", "value"),//dictInfo.fields,
                        "info" to mapOf(
                                "type" to dictInfo.dictType,
                                "name" to dictInfo.dictName,
                                "total" to dictInfo.itemList.count(),
                                "source" to dictInfo.fields.last()
                        ),
                        "lines" to dictInfo.itemList.drop(start ?: 0).take(count ?: 100)
                )
        )
    }

    @ApiOperation("update dictionary - not supported dict creation")
    @ApiKeySecured
    @PostMapping("/dictionary/operation")
    fun updateDictionary(@RequestParam("workspaceId") workspaceId: Long,
                         @RequestParam("revisionId", required = false) revisionId: Long?,
                         @RequestParam("variant") variant: String,
                         @RequestParam("language") language: String,
                         @RequestParam("appName") appName: String,
                         @Valid @RequestBody dataFormat: DictDataFormat): Any {
        val modelId = LanguageModelDictId(workspaceId, revisionId, variant, language, appName, dataFormat.dictType, dataFormat.dictName)

        languageModelManagementService.updateLanguageModelDictionaryItems(modelId, dataFormat.sourceField, dataFormat.mode
                ?: DataUpdateMode.update, dataFormat.dataList)

        return mapOf("dictionary" to mapOf(
                "status" to "success"
        ))
    }

    @ApiOperation("delete dictionary")
    @ApiKeySecured
    @DeleteMapping("/dictionary/operation")
    fun deleteDictionary(@RequestParam("workspaceId") workspaceId: Long,
                         @RequestParam("revisionId", required = false) revisionId: Long?,
                         @RequestParam("variant") variant: String,
                         @RequestParam("language") language: String,
                         @RequestParam("appName") appName: String,
                         @Valid @RequestBody deleteFormat: DictDataDeleteFormat): Any {

        val modelId = LanguageModelDictId(workspaceId, revisionId, variant, language, appName, deleteFormat.dictType, deleteFormat.dictName)
        languageModelManagementService.deleteLanguageModelDictionary(modelId)

        return mapOf("dictionary" to mapOf(
                "status" to "success"
        ))
    }

    @ApiOperation("find dictionary item by value")
    @ApiKeySecured
    @PostMapping("/dictionary/operation/find")
    fun findDictionaryItemByValue(@RequestParam("workspaceId") workspaceId: Long,
                                  @RequestParam("revisionId", required = false) revisionId: Long?,
                                  @RequestParam("variant") variant: String,
                                  @RequestParam("language") language: String,
                                  @RequestParam("appName") appName: String,
                                  @Valid @RequestBody findFormat: DictDataFindFormat): Any {
        val modelId = LanguageModelDictId(workspaceId, revisionId, variant, language, appName, findFormat.dictType, findFormat.dictName)
        val dictInfo = languageModelManagementService.findLanguageModelDictionary(modelId)
        val findString = findFormat.findValue.toLowerCase()

        val foundList = dictInfo.itemList.filter {
            it.data.any {
                it.toLowerCase().contains(findString)
            }
        }.map {
            it.data
        }

        return mapOf("fields" to listOf("source", "value"),//dictInfo.fields,
                "lines" to foundList)
    }

    @GetMapping(value = ["/helper/asr/entityfinder", "/entity/operation/find"])
    fun findEntity(@RequestParam("workspaceId") workspaceId: Long,
                   @RequestParam("revisionId", required = false) revisionId: Long?,
                   @RequestParam("variant") variant: String,
                   @RequestParam("language") language: String,
                   @RequestParam("appName", required = false) appName: String?,
                   @RequestParam("dataName", required = false) dataName: String?,
                   @RequestParam("query") query: String) : Any {
        return sendResult(
                matcherService.findEntity(workspaceId, revisionId, variant, language, appName, dataName, query)
        )
    }

    @GetMapping("/helper/match")
    fun getMatchInWorkspace(
            @RequestParam("workspaceId") workspaceId: Long,
            @RequestParam("revisionId", required = false) revisionId: Long?,
            @RequestParam("variant") variant: String,
            @RequestParam("language") language: String,
            @RequestParam("appName", required = false) appName: String?,
            @RequestParam("phrase") phrase: String): Any {
        return sendResult(
                matcherService.findPhrase(workspaceId, revisionId, variant, language, appName, phrase)
        )
    }

    //@PostMapping(value = ["/helper/generator/match"])
    //fun getMatch(
    //        @RequestParam("workspaceId") workspaceId: Long,
    //        @RequestParam("revisionId", required = false) revisionId: Long?,
    //        @RequestParam("variant") variant: String,
    //        @RequestParam("language") language: String,
    //        @RequestParam("phrase") phrase: String,
    //        @Valid @RequestBody patterns: List<String>): Any {
    //
    //    val matcher = asrContextRepository?.getMatcher(workspaceId, revisionId, variant, language, patterns)!!
    //
    //    val result = matcher.match(phrase)
    //
    //    return view(result)
    //}
    //
    //@GetMapping("/helper/generator/expander")
    //fun getExpandedString(
    //        @RequestParam("workspaceId") workspaceId: Long,
    //        @RequestParam("revisionId", required = false) revisionId: Long?,
    //        @RequestParam("variant") variant: String,
    //        @RequestParam("language") language: String,
    //        @RequestParam("pattern") pattern: String): Any {
    //
    //    val dictionaryManager = asrContextRepository?.getDictionaryManager(workspaceId, revisionId, variant, language)?.dictManager
    //
    //    val expandedString = dictionaryManager?.extractMacro(pattern) ?: ""
    //    val extractedString = PatternToNodeUtil.patternToStrings(expandedString)
    //
    //    return view(mapOf("variant" to variant, "language" to language, "pattern" to pattern, "expanded" to expandedString, "extracted" to extractedString))
    //}
    //
    //@GetMapping("/helper/generator/guess")
    //fun getGuessPhrase(
    //        @RequestParam("workspaceId") workspaceId: Long,
    //        @RequestParam("revisionId", required = false) revisionId: Long?,
    //        @RequestParam("variant") variant: String,
    //        @RequestParam("language") language: String,
    //        @RequestParam("phrase") phrase: String): Any {
    //
    //    val generator = asrContextRepository?.getExpressionGenerator(workspaceId, revisionId, variant, language)!!
    //    val guesses = generator.guess(phrase)
    //
    //    return view(mapOf("variant" to variant, "language" to language, "phrase" to phrase, "guess" to guesses))
    //}
    //
    //class GenerateEntity {
    //    val begin: Int = 0
    //    val end: Int = 0
    //    val name: String = ""
    //}
    //
    //@PostMapping("/helper/generator/pattern")
    //fun getGeneratePattern(
    //        @RequestParam("workspaceId") workspaceId: Long,
    //        @RequestParam("revisionId", required = false) revisionId: Long?,
    //        @RequestParam("variant") variant: String,
    //        @RequestParam("language") language: String,
    //        @RequestParam("phrase") phrase: String,
    //        @Valid @RequestBody params: List<GenerateEntity>): Any {
    //
    //    val generator = asrContextRepository?.getExpressionGenerator(workspaceId, revisionId, variant, language)!!
    //    val selectEntities = params.map {
    //        EntityExtractor.EntityResult(it.begin, it.end, it.name, 1.0f)
    //    }
    //    val result = generator.select(phrase, selectEntities)
    //    val generated = generator.generate(result)
    //    val expanded = generated.map { generator.expand(it) }
    //
    //    return view(mapOf("variant" to variant, "language" to language, "selected" to selectEntities, "generated" to generated, "expanded" to expanded))
    //}
    //
}