/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.repository.datamodel
      Date  : 19. 3. 12
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.repository.datamodel

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.io.File
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class LanguageModelDictionary(
        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0,

        var name: String = "",
        var fileDataKey: String = "",
        var headers: String = "",

        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @OnDelete(action = OnDeleteAction.CASCADE)
        @JsonIgnore
        private var languageModel: LanguageModel? = null

) {
    companion object {
        val baseDir = "dictionaries"
        fun createDictionaryModel(parentModel: LanguageModel, dataKey: String, dataFile: File,
                                  onDictionary: (model: LanguageModelDictionary) -> Unit,
                                  onDictionaryItem: (model: LanguageModelDictionary, data: String) -> Unit): LanguageModelDictionary {
            val data = dataFile.bufferedReader().use { it.readLines() }
            val header = data.first()
            val dictionaryModel = LanguageModelDictionary(fileDataKey = dataKey, languageModel = parentModel, headers = header, name = dataFile.name)

            onDictionary(dictionaryModel)

            for (item in data.drop(1)) {
                if (item.startsWith("#") || item.isBlank() || item.isEmpty())
                    continue
                onDictionaryItem(dictionaryModel, item)
            }

            return dictionaryModel
        }

        fun writeDictionaryModel(dataDir: File, dictModel: LanguageModelDictionary, dictItems: List<LanguageModelDictionaryItem>) {
            val dstFile = File(dataDir, "$baseDir/${dictModel.name}")
            if (!dstFile.parentFile.isDirectory)
                dstFile.parentFile.mkdirs()

            dstFile.bufferedWriter().use { bw ->
                bw.write(dictModel.headers + "\n")
                dictItems.forEach {
                    bw.write(it.dataItem + "\n")
                }
            }
        }

        fun createLanguageModelDict(name: String, fileDataKey: String, headers: String, model: LanguageModel): LanguageModelDictionary {
            return LanguageModelDictionary(name = name, fileDataKey = fileDataKey, headers = headers, languageModel = model)
        }

        fun loadDictionaryItemList(dataPath: String, dict: LanguageModelDictionary): List<String>? {
            if (dict.fileDataKey.isEmpty()) return null

            return File(File(dataPath), dict.fileDataKey).bufferedReader().use { it.readLines().drop(1) }
        }

    }
}
