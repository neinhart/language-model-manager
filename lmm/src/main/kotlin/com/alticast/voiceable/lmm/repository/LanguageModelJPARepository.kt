/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.repository.datamodel
      Date  : 19. 3. 12
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.repository

import com.alticast.voiceable.lmm.repository.datamodel.LanguageModel
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelDictionary
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelDictionaryItem
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelEntity
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelEntityItem
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelPrivate
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelRevision
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelStory
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelStoryItem
import com.alticast.voiceable.lmm.repository.datamodel.LanguageModelWorkspace
import com.alticast.voiceable.lmm.repository.datamodel.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface LanguageModelRepository: JpaRepository<LanguageModel, Long> {
    fun findAllByLanguageModelRevisionId(revisionId: Long): List<LanguageModel>?
    fun findByLanguageModelRevisionIdAndVariantAndLanguageAndAppName(revisionId: Long, variant: String, language: String, appName: String): LanguageModel?
    fun findAllByLanguageModelRevisionIdAndVariantAndLanguage(revisionId: Long, variant: String, language: String): List<LanguageModel>?
}

@Repository
interface LanguageModelDictionaryRepository: JpaRepository<LanguageModelDictionary, Long> {
    fun findAllByLanguageModelId(languageModelId: Long): List<LanguageModelDictionary>?
}

@Repository
interface LanguageModelDictionaryItemRepository: JpaRepository<LanguageModelDictionaryItem, Long> {
    fun findAllByLanguageModelDictionaryId(dictId: Long): List<LanguageModelDictionaryItem>?
}

@Repository
interface LanguageModelEntityRepository: JpaRepository<LanguageModelEntity, Long> {
    fun findAllByLanguageModelId(languageModelId: Long): List<LanguageModelEntity>?
    fun findAllByLanguageModelIdAndName(languageModelId: Long, dataName: String): List<LanguageModelEntity>?
}

@Repository
interface LanguageModelEntityItemRepository: JpaRepository<LanguageModelEntityItem, Long> {
    fun findAllByLanguageModelEntityId(entityId: Long): List<LanguageModelEntityItem>?
    fun deleteByIdIn(idList: List<Long>)
    fun findAllByIdIn(idList: List<Long>): List<LanguageModelEntityItem>?
}

@Repository
interface LanguageModelRevisionRepository: JpaRepository<LanguageModelRevision, Long> {
    fun findAllByLanguageModelWorkspaceId(workspaceId: Long): List<LanguageModelRevision>?
}

@Repository
interface LanguageModelStoryRepository: JpaRepository<LanguageModelStory, Long> {
    fun findAllByLanguageModelId(languageModelId: Long): List<LanguageModelStory>?
    fun findAllByLockById(userId: Long): List<LanguageModelStory>?
}

@Repository
interface LanguageModelStoryItemRepository: JpaRepository<LanguageModelStoryItem, Long> {
    fun findAllByLanguageModelStoryId(storyId: Long): List<LanguageModelStoryItem>?
}

@Repository
interface LanguageModelWorkspaceRepository: JpaRepository<LanguageModelWorkspace, Long> {
    fun findByEditable(editable: Boolean): LanguageModelWorkspace?
}

@Repository
interface LanguageModelPrivateRepository: JpaRepository<LanguageModelPrivate, Long> {
    fun findAllByLanguageModelId(languageModelId: Long): List<LanguageModelPrivate>?
}

@Repository
interface UserRepository: JpaRepository<User, Long> {
    fun existsByEmail(email: String): Boolean
    fun existsByUserId(userid: String): Boolean
    fun findByEmail(email: String): User?
    fun findByToken(token: String): User?
    fun findByUsername(username: String): User?
    fun findByUserId(userId: String): User?
}

