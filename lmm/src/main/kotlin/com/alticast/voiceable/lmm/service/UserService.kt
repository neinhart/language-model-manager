/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.service
      Date  : 19. 3. 12
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.service

import com.alticast.voiceable.lmm.exceptions.InvalidUserException
import com.alticast.voiceable.lmm.repository.UserRepository
import com.alticast.voiceable.lmm.repository.datamodel.User
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.mindrot.jbcrypt.BCrypt
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.util.Date

@Service
class UserService(val userRepository: UserRepository,
                  @Value("\${jwt.secret}") val jwtSecret: String,
                  @Value("\${jwt.issuer}") val jwtIssuer: String) {

    private val logger = LoggerFactory.getLogger(UserService::class.java)
    private val currentUser = ThreadLocal<User>()

    fun findByToken(token: String) = userRepository.findByToken(token)
    fun findByUsername(username: String) = userRepository.findByUsername(username)
    fun findByUserId(userId: String) = userRepository.findByUserId(userId)
    fun clearCurrentUser() = currentUser.remove()
    fun setCurrentUser(user: User): User {
        val registerUser = userRepository.findByUsername(user.username) ?: let {
            userRepository.save(user)
        }

        currentUser.set(registerUser)

        return registerUser
    }

    fun currentUser() = currentUser.get()
    fun newToken(user: User): String {
        return Jwts.builder()
                .setIssuedAt(Date())
                .setSubject(user.email)
                .setIssuer(jwtIssuer)
                .setExpiration(Date(System.currentTimeMillis() + 10 * 24 * 60 * 60 * 1000)) // 10 days
                .signWith(SignatureAlgorithm.HS256, jwtSecret).compact()
    }

    fun validToken(token: String, user: User): Boolean {
        val claims = Jwts.parser().setSigningKey(jwtSecret)
                .parseClaimsJws(token).body
        return claims.subject == user.email && claims.issuer == jwtIssuer
                && Date().before(claims.expiration)
    }

    //@CachePut(cacheNames=arrayOf("usersByToken"), key="#user.token")
    fun updateToken(user: User): User {
        user.token = newToken(user)
        return userRepository.save(user)
    }

    fun login(userId: String, password: String): User? {
        checkAndMakeAdminUser()

        val user = userRepository.findByUserId(userId) ?: throw InvalidUserException("Can't find user id ($userId)")

        if (BCrypt.checkpw(password, user.password)) return updateToken(user)

        throw InvalidUserException("Login failed - Invalid Password")
    }

    fun removeUser(user: User) {
        userRepository.delete(user)
    }

    @Value("\${lmm.password}")
    private val adminPassword: String = "1234"

    fun checkAndMakeAdminUser(): User {
        return userRepository.findByUserId("admin") ?: let {
            val password = BCrypt.hashpw(adminPassword, BCrypt.gensalt())
            User(username = "관리자다", userId = "admin",
                    email = "admin@voiceable.com", role = "ADMIN", password = password).apply {
                userRepository.save(this)
            }
        }
    }

    fun isAdminUser(user: User): Boolean {
        return user.userId == "admin"
    }

    fun updateWorkspace(workspaceId: Long, user: User) {
        user.recently.removeIf { it == workspaceId }
        user.recently.add(workspaceId)
        if (user.recently.count() > 10)
            user.recently.removeAt(0)

        userRepository.save(user)
    }

    fun existsUser(userId: String, email: String): Boolean {
        return userRepository.existsByEmail(email) || userRepository.existsByUserId(userId)
    }

    fun registerUser(userId: String, email: String, userName: String, password: String, role: String?): User {
        val newUser = User(userId = userId, username = userName, email = email, role = role ?: "",
                password = BCrypt.hashpw(password, BCrypt.gensalt()))

        newUser.token = newToken(newUser)

        return userRepository.save(newUser)
    }

    fun updateUserInfo(userId: String, email: String?, userName: String?, password: String?, role: String?): User {
        val currentUser = currentUser()
        val user = userRepository.findByUserId(userId) ?:
                throw InvalidUserException("Can't find user id - $userId")

        email?.let {
            user.email = email
        }

        userName?.let {
            user.username = userName
        }

        password?.let {
            if (it.isNotEmpty())
                user.password = BCrypt.hashpw(password, BCrypt.gensalt())
        }

        role?.let {
            user.role = role
        }

        if (user.userId != currentUser.userId)
            user.token = newToken(user)

        user.updateDate = LocalDateTime.now()

        return userRepository.save(user)
    }

    fun findAllUser(): List<User> {
        return userRepository.findAll()
    }
}
