/*
    Copyright (C) 2018 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm
      Date  : 18. 11. 5
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.utils

import org.apache.tomcat.util.http.fileupload.IOUtils
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.nio.file.Files
import java.nio.file.Files.createDirectories
import java.nio.file.Paths
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import java.util.zip.ZipOutputStream




object ZipDirectory {
    fun packToZip(sourceDirPath: String, zipFilePath: String) {
        File(zipFilePath).let { if (it.exists()) it.delete() }

        val zipFile = Files.createFile(Paths.get(zipFilePath))

        ZipOutputStream(Files.newOutputStream(zipFile)).use { stream ->
            val sourceDir = Paths.get(sourceDirPath)
            Files.walk(sourceDir).filter { path -> !Files.isDirectory(path) }.forEach { path ->
                val zipEntry = ZipEntry(path.toString().substring(sourceDir.toString().length + 1))

                stream.putNextEntry(zipEntry)
                stream.write(Files.readAllBytes(path))
                stream.closeEntry()
            }
        }
    }

    fun unpackFromZip(sourceZipStream: InputStream, outputFolder: File) {
        if (outputFolder.isDirectory)
            outputFolder.deleteRecursively()

        createDirectories(outputFolder.toPath())

        ZipInputStream(sourceZipStream).use { zipInputStream ->
            var nextEntry: ZipEntry? = zipInputStream.nextEntry

            while (nextEntry != null) {
                val entryFile = File(outputFolder, nextEntry.name)
                if (nextEntry.isDirectory) {
                    entryFile.mkdirs()
                } else {
                    if (entryFile.parentFile != null && !entryFile.parentFile.exists()) {
                        entryFile.parentFile.mkdirs()
                    }

                    entryFile.createNewFile()
                    entryFile.outputStream().use {
                        IOUtils.copy(zipInputStream, it)
                    }
                }

                nextEntry = zipInputStream.nextEntry
            }
        }
    }

    @Throws(IOException::class)
    private fun writeFile(inputStream: ZipInputStream, file: File) {
        val buffer = ByteArray(1024)
        FileOutputStream(file).use { fileOutputStream ->
            var length: Int
            do {
                length = inputStream.read(buffer)
                if (length > 0)
                    fileOutputStream.write(buffer, 0, length)
            } while (length > 0)
        }
    }
}

