/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.utils
      Date  : 19. 3. 13
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.utils

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValues
import java.io.File

data class StoryCondition(val url: String? = null, val text: String? = null, val voiceableContext: String? = null, val tag: String? = null) {
    //fun getCondition(): String {
    //    return "[url:${url ?: ""}|" +
    //            "text:${text ?: ""}|" +
    //            "voiceableContext:${voiceableContext ?: ""}|" +
    //            "tag:${tag ?: ""}]"
    //}
}

data class StoryIntent(val id: Int?, val pattern: String?, val action: String?, val alias: String?,
                       val matchClass: String?, val hints: List<String>?, val close: Boolean?, val name: String?,
                       val patterns: List<String>?, val priority: Int?)

data class StoryComponentPriority(val left: Float?, val right: Float?, val top: Float?, val bottom: Float?,
                                  val focus: Float?, val word: Float?, val itemCount: Float?, val size: Float?)

data class StoryContext(val condition: StoryCondition?, val conditions: List<StoryCondition>?,
                        val intent: StoryIntent?, val intents: MutableList<StoryIntent>?,
                        val alias: String?, val aliases: List<String>?,
                        val componentPriority: StoryComponentPriority?,
                        val name: String?, val tag: String?, val tags: List<String>?,
                        val timeout: Int?, val priority: Int?) {
    @JsonIgnore
    fun getContextName() = name ?: "unknown"
    @JsonIgnore
    fun getContextPriority() = priority ?: -1
    @JsonIgnore
    fun getContextConditions(): List<StoryCondition> {
        val conditionList = mutableListOf<StoryCondition>()
        condition?.run { conditionList.add(this) }
        conditions?.run { conditionList.addAll(this) }

        return conditionList
    }

    @JsonIgnore
    fun getContextTags(): List<String> {
        val tagList = mutableListOf<String>()
        tag?.run { tagList.add(this) }
        tags?.run { tagList.addAll(this) }

        return tagList
    }

    @JsonIgnore
    fun getContextTimeout() = timeout ?: -1

    @JsonIgnore
    fun getContextIntents(): List<StoryIntent> {
        val intentList = mutableListOf<StoryIntent>()
        intent?.run { intentList.add(this) }
        intents?.run { intentList.addAll(this) }

        return intentList
    }
}

data class Story(val context: StoryContext?, val contexts: List<StoryContext>?, val intent: StoryIntent?)

object StoryParser {
    fun parse(f: File): List<StoryContext> {
        val objectMapper = ObjectMapper(YAMLFactory()).registerModule(KotlinModule())
        val yamlParser = YAMLFactory().createParser(f)

        val contextList = mutableListOf<StoryContext>()
        objectMapper.readValues<Story>(yamlParser).readAll().forEach { story ->
            if (story.contexts != null)
                contextList.addAll(story.contexts)
            if (story.context != null)
                contextList.add(story.context)
        }

        return contextList
    }


    fun buildYaml(f: File, story: Story) {
        ObjectMapper(YAMLFactory()).setSerializationInclusion(JsonInclude.Include.NON_NULL)
                .writerWithDefaultPrettyPrinter().writeValue(f, story)
    }
}