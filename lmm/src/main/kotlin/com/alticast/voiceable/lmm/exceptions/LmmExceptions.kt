/*
    Copyright (C) 2019 Alticast Corporation. All Rights Reserved.
    
    This software is the confidential and proprietary information
    of Alticast Corporation. You may not use or distribute
    this software except in compliance with the terms and conditions
    of any applicable license agreement in writing between Alticast
    Corporation and you.

    Package : com.alticast.voiceable.lmm.exceptions
      Date  : 19. 3. 14
     Author : kimjh 
*/
package com.alticast.voiceable.lmm.exceptions

class InvalidWorkspaceException(val workspaceId: Long, val reason: String) : RuntimeException()
class InvalidRevisionException(val workspaceId: Long, val revision: Long?, val reason: String) : RuntimeException()
class InvalidUserException(val reason: String) : RuntimeException()
class InvalidLanguageModelException(val reason: String) : RuntimeException()
class InvalidLanguageModelContextException(val reason: String): java.lang.RuntimeException()