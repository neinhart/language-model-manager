package com.alticast.voiceable.lmm.controller

import com.alticast.voiceable.lmm.utils.ZipDirectory
import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.core.io.ClassPathResource
import org.springframework.test.context.junit4.SpringRunner
import java.io.File

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class UpdateRestControllerTest {

    @Autowired
    lateinit var testRestTemplate: TestRestTemplate

    @Autowired
    lateinit var updateRestController: UpdateRestController

    @Before
    fun setUp() {
    }

    val tmpSrcDir = createTempDir()

    fun prepareSrcEntities() {
        tmpSrcDir.deleteRecursively()
        val testlmzip = ClassPathResource("testlm.zip").inputStream
        ZipDirectory.unpackFromZip(testlmzip, tmpSrcDir)
        assertTrue(File(tmpSrcDir,"testlm").exists())
    }

    @Test
    fun updateEntities() {

        prepareSrcEntities()

        assertFalse(File(updateRestController.workingPath, "e2etest").exists())

        val variant = "e2etest"
        val language="KO_KR"
        val url = "/operation/update_entities/$variant/$language"

        val res = testRestTemplate.getForEntity(url, String::class.java)

        assertTrue(File(updateRestController.workingPath, "e2etest").exists())
//        assertTrue(File(workingPath, "e2etest").exists())
    }
}
