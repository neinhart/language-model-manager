package com.alticast.voiceable.lmm.controller

import com.alticast.voiceable.lmm.repository.LanguageModelRevisionRepository
import com.alticast.voiceable.lmm.repository.LanguageModelWorkspaceRepository
import com.fasterxml.jackson.databind.ObjectMapper
import junit.framework.TestCase.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.core.env.Environment
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class WorkspaceRestControllerTest {

    @Autowired
    lateinit var testRestTemplate: TestRestTemplate

    @Autowired
    lateinit var environment: Environment

    @Autowired
    lateinit var languageModelWorkspaceRepository: LanguageModelWorkspaceRepository

    @Autowired
    lateinit var languageModelRevisionRepository: LanguageModelRevisionRepository

    @Before
    fun setUp() {
        languageModelWorkspaceRepository.deleteAll()
        languageModelRevisionRepository.deleteAll()
    }

    @Test
    fun createWorkspace() {
        val body = mapOf(Pair("workspaceId", "1818"), Pair("revision","1"), Pair("name","test"), Pair("filterVariants",null), Pair("filterLanguage",null))

        assertEquals(0, languageModelWorkspaceRepository.count())

        val res = testRestTemplate.postForEntity("/workspace", body, String::class.java) // WS 생성

        assertEquals(1, languageModelWorkspaceRepository.count())
    }

    @Test
    fun createRevision() {
        val body = mapOf(Pair("workspaceId", "1818"), Pair("revision","1"), Pair("name","test"), Pair("filterVariants",null), Pair("filterLanguage",null))

        // WS 생성
        val res = testRestTemplate.postForEntity("/workspace", body, String::class.java) // WS 생성
        val createdWorkspaceId = (ObjectMapper().readValue(res.body, Map::class.java).get("workspace") as Map<String,*>).get("id")

        // revision history 조회해서 revision id 받아온다
        val res1_a = testRestTemplate.getForEntity( "/workspace/$createdWorkspaceId/rev/list", String::class.java)
        val hist1 = ObjectMapper().readValue(res1_a.body, Map::class.java).get("history") as ArrayList<*>
        val revid1 = (hist1.get(0) as Map<String,Int>).get("id")!!

        assertEquals(1, languageModelWorkspaceRepository.count())
        assertEquals(1, languageModelRevisionRepository.count())

        // WS 또 생성
        val body2 = mapOf(Pair("workspaceId", "$createdWorkspaceId"), Pair("revision","$revid1"))
        val res2 = testRestTemplate.postForEntity("/workspace", body2, String::class.java)
        assertEquals(2, languageModelWorkspaceRepository.count())  // 워크스페이스는 기존것 엎어침
        assertEquals(2, languageModelRevisionRepository.count())
    }

    @Test
    fun createRevisionWithWrongRevId() {
        val createdWorkspaceId = createAndGetWorkspaceId()
        val revid1 = getRevisionId(createdWorkspaceId)

        assertEquals(1, languageModelWorkspaceRepository.count())
        assertEquals(1, languageModelRevisionRepository.count())

        // WS 또 생성 : 이번엔 잘못된 rev id 를 넘긴다!!!
        val revid2 = revid1+9999
        val body2 = mapOf(Pair("workspaceId", "$createdWorkspaceId"), Pair("revision","$revid2"))
        val res2 = testRestTemplate.postForEntity("/workspace", body2, String::class.java)
        assertEquals(1, languageModelWorkspaceRepository.count())  // 워크스페이스는 기존것 엎어침
        assertEquals(1, languageModelRevisionRepository.count())
    }

    @Test
    fun getWorkspaceList() {
        val w1 = createAndGetWorkspaceId()
        val w2 = createAndGetWorkspaceId()
        val res = testRestTemplate.getForEntity("/workspace?ids=$w1,$w2", String::class.java)
        val workspaces = (ObjectMapper().readValue(res.body, Map::class.java).get("workspace") as List<Map<String,Int>>)!!
        assertEquals(w1, (workspaces.get(0) as Map<String, Int>).get("id"))
        assertEquals(w2, (workspaces.get(0) as Map<String, Int>).get("id"))
    }

    fun createAndGetWorkspaceId():Int {
        val body = mapOf(Pair("workspaceId", "1818"), Pair("revision","1"), Pair("name","test"), Pair("filterVariants",null), Pair("filterLanguage",null))

        // WS 생성
        val res = testRestTemplate.postForEntity("/workspace", body, String::class.java) // WS 생성
        return (ObjectMapper().readValue(res.body, Map::class.java).get("workspace") as Map<String,Int>).get("id")!!
    }


    fun getRevisionId( workspaceId:Int ): Int {
        val res1_a = testRestTemplate.getForEntity( "/workspace/$workspaceId/rev/list", String::class.java)
        val hist1 = ObjectMapper().readValue(res1_a.body, Map::class.java).get("history") as ArrayList<*>
        return (hist1.get(0) as Map<String,Int>).get("id")!!
    }

    @Test
    fun deleteWorkspace() {
        assertEquals(0, languageModelWorkspaceRepository.count())

        val body = mapOf(Pair("workspaceId", "1"), Pair("revision","1"), Pair("name","test"), Pair("filterVariants",null), Pair("filterLanguage",null))
        testRestTemplate.postForEntity("/workspace", body, String::class.java)
        assertEquals(1, languageModelWorkspaceRepository.count())
        val id = languageModelWorkspaceRepository.findAll().get(0).id

        // 없는 ID
        testRestTemplate.delete("/workspace/${id+1}", body, String::class.java)
        assertEquals(1, languageModelWorkspaceRepository.count())

        testRestTemplate.delete("/workspace/$id", body, String::class.java)
        assertEquals(0, languageModelWorkspaceRepository.count())
    }
}