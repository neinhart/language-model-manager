package com.alticast.voiceable.lmm.controller

import com.alticast.voiceable.lmm.repository.UserRepository
import junit.framework.Assert.assertEquals
import junit.framework.TestCase
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class UserRestControllerTest {

    @Autowired
    lateinit var testRestTemplate: TestRestTemplate

    @Autowired
    lateinit var userRepository: UserRepository

    @Before
    fun setUp() {
        userRepository.deleteAll()
    }

    @Test
    fun register() {

        assertEquals(0, userRepository.findAll().size)

        val body = mapOf(Pair("userId", "foo"), Pair("username","bar"), Pair("password","baz"), Pair("role","admin"), Pair("email","foo@example.com"))

        val res = testRestTemplate.postForEntity("/user", body, String::class.java)

        // AutoGenUser 가 있음. ApiKeySecuredAspect.findUserByApiKey() 참고
        // TODO AutoGen User 를 빼고 초기 Admin 계정이 자동 생성되도록 변경
        assertEquals(2, userRepository.findAll().size)
    }

    @Test
    fun registerTwice() {
        register()

        val body = mapOf(Pair("userId", "foo"), Pair("username","bar"), Pair("password","baz"), Pair("role","admin"), Pair("email","foo@example.com"))

        val res = testRestTemplate.postForEntity("/user", body, String::class.java)

        assertEquals(2, userRepository.findAll().size)
    }
}
