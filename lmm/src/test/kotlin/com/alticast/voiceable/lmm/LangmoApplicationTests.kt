package com.alticast.voiceable.lmm

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import java.util.Date

@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(LangmoApplication::class), webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = arrayOf("test"))
class LangmoApplicationTests {
    private val logger = LoggerFactory.getLogger(LangmoApplicationTests::class.java)
    @Autowired
    lateinit var testRestTemplate: TestRestTemplate
    private val mapper = ObjectMapper()

    data class Workspace(@JsonInclude(JsonInclude.Include.NON_NULL) val name: String? = null,
                         @JsonInclude(JsonInclude.Include.NON_NULL) val deploy: Boolean? = null,
                         @JsonInclude(JsonInclude.Include.NON_NULL) val id: Long? = null,
                         @JsonInclude(JsonInclude.Include.NON_NULL) val updated: Date? = null)

    data class WorkspaceView(val workspace: Workspace? = null)
    data class WorkspaceList(val workspace: List<Workspace>? = null)

    private fun getWorkspaces(): List<Workspace>? {
        val workspaces = testRestTemplate.getForObject("/workspace", WorkspaceList::class.java).workspace

        logger.debug("workspaces")
        workspaces?.forEach {
            logger.debug(it.toString())
        }
        return workspaces
    }

    private fun createWorkspace(name: String, id: Long) =
            testRestTemplate.postForObject("/workspace", mapOf("name" to name, "workspaceId" to id),
                    WorkspaceView::class.java).workspace

    private fun deleteWorkspace(id: Long) = testRestTemplate.delete("/workspace/$id")
    private fun changeWorkspace(id: Long, name: String) = testRestTemplate.put("/workspace/$id", mapOf("name" to name), WorkspaceView::class.java)

    @Test
    fun contextLoads() {
        logger.info("Create / Delete Workspace Test")

        val deployWorkspaceId = getWorkspaces()?.find { it.deploy == true }?.id
        deployWorkspaceId ?: assert(false)
        logger.info("deploy workspace id : $deployWorkspaceId")
        val newId = createWorkspace("newTestWorkspace", deployWorkspaceId!!)?.id
        newId ?: assert(false)
        changeWorkspace(newId!!, "NewName")
        assert(getWorkspaces()?.any { it.name == "NewName" } ?: false)
        deleteWorkspace(newId)
        assert(getWorkspaces()?.none { it.id == newId } ?: false)
    }
}
